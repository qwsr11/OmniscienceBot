﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OmniscienceBotCore.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ChannelSettings",
                columns: table => new
                {
                    Channel = table.Column<string>(type: "TEXT", nullable: false),
                    GoogleSpreadsheet = table.Column<string>(type: "TEXT", nullable: true),
                    Active = table.Column<bool>(type: "INTEGER", nullable: false),
                    SilenceMode = table.Column<bool>(type: "INTEGER", nullable: false),
                    FollowReactionFormat = table.Column<string>(type: "TEXT", nullable: true),
                    NewSubReactionFormat = table.Column<string>(type: "TEXT", nullable: true),
                    ReSubReactionFormat = table.Column<string>(type: "TEXT", nullable: true),
                    GiftedSubReactionFormat = table.Column<string>(type: "TEXT", nullable: true),
                    SpotifyRefreshToken = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChannelSettings", x => x.Channel);
                });

            migrationBuilder.CreateTable(
                name: "Commands",
                columns: table => new
                {
                    Channel = table.Column<string>(type: "TEXT", nullable: false),
                    Key = table.Column<string>(type: "TEXT", nullable: false),
                    Role = table.Column<int>(type: "INTEGER", nullable: false),
                    Period = table.Column<int>(type: "INTEGER", nullable: false),
                    Discriminator = table.Column<string>(type: "TEXT", nullable: false),
                    Path = table.Column<string>(type: "TEXT", nullable: true),
                    Duration = table.Column<int>(type: "INTEGER", nullable: true),
                    Text = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Commands", x => new { x.Channel, x.Key });
                });

            migrationBuilder.CreateTable(
                name: "Counters",
                columns: table => new
                {
                    Channel = table.Column<string>(type: "TEXT", nullable: false),
                    Key = table.Column<string>(type: "TEXT", nullable: false),
                    HandleMode = table.Column<int>(type: "INTEGER", nullable: false),
                    TextToFind = table.Column<string>(type: "TEXT", nullable: true),
                    ResponseMode = table.Column<int>(type: "INTEGER", nullable: false),
                    ResponseParams = table.Column<string>(type: "TEXT", nullable: true),
                    AnswerPattern = table.Column<string>(type: "TEXT", nullable: true),
                    Count = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Counters", x => new { x.Channel, x.Key });
                });

            migrationBuilder.CreateTable(
                name: "Events",
                columns: table => new
                {
                    Channel = table.Column<string>(type: "TEXT", nullable: false),
                    Key = table.Column<string>(type: "TEXT", nullable: false),
                    Active = table.Column<bool>(type: "INTEGER", nullable: false),
                    Json = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Events", x => new { x.Channel, x.Key });
                });

            migrationBuilder.CreateTable(
                name: "Pastes",
                columns: table => new
                {
                    Channel = table.Column<string>(type: "TEXT", nullable: false),
                    Key = table.Column<string>(type: "TEXT", nullable: false),
                    Text = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pastes", x => new { x.Channel, x.Key });
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ChannelSettings");

            migrationBuilder.DropTable(
                name: "Commands");

            migrationBuilder.DropTable(
                name: "Counters");

            migrationBuilder.DropTable(
                name: "Events");

            migrationBuilder.DropTable(
                name: "Pastes");
        }
    }
}
