﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OmniscienceBotCore.DataManagement
{
    public class ResourceDispatcher
    {
        Dictionary<string, string> resources;
        public ResourceDispatcher(string folder)
        {
            Init(folder);
        }

        void Init(string folder)
        {
            resources = ByPass(folder);
        }

        Dictionary<string, string> ByPass(string folder)
        {
            Dictionary<string, string> res = new Dictionary<string, string>();
            foreach (var f in Directory.EnumerateDirectories(folder))
                foreach (var subf in ByPass(f))
                    res.Add(subf.Key, subf.Value);

            foreach (var file in Directory.EnumerateFiles(folder))
                res.Add(file.Substring(file.LastIndexOf('\\') + 1), folder);

            return res;
        }

        public void Add(string key, string path)
        {
            if (resources.ContainsKey(key)) throw new ArgumentException("Данный ключ уже используется.");
            resources.Add(key, path);
        }

        public void Replace(string key, string newPath)
        {
            if (!resources.ContainsKey(key)) throw new ArgumentException("Данного ключа не существует.");
            resources[key] = newPath;
        }

        public void Remove(string key)
        {
            if (!resources.ContainsKey(key)) throw new ArgumentException("Данного ключа не существует.");
            resources.Remove(key);
        }
    }
}