﻿using Microsoft.EntityFrameworkCore;
using OmniscienceBotCore.Models;
using OmniscienceBotCore.Models.Commands;
using OmniscienceBotCore.Models.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OmniscienceBotCore.DataManagement
{
    public class DataContext : DbContext
    {
        public DbSet<Command> Commands { get; set; }
        public DbSet<StreamAnnouncement> StreamAnnouncements { get; set; }
        public DbSet<ChannelSettings> ChannelSettings { get; set; }
        public DbSet<Counter> Counters { get; set; }
        public DbSet<Paste> Pastes { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Moderator> ChannelModerators { get; set; }
        public DbSet<EntityChange> EntitiesChangelog { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TextCommand>();
            modelBuilder.Entity<Command>().HasKey(c => c.Id);

            modelBuilder.Entity<StreamAnnouncement>().HasKey(c => c.Id);
            modelBuilder.Entity<Counter>().HasKey(c => c.Id);
            modelBuilder.Entity<Paste>().HasKey(c => c.Id);
            modelBuilder.Entity<Event>().HasKey(c => c.Id);
            modelBuilder.Entity<EntityChange>().HasKey(c => c.Id);


            modelBuilder.Entity<ChannelsGlobalEvents>().HasKey(cge => new { cge.Id, cge.Channel });

            modelBuilder.Entity<ChannelsGlobalEvents>()
                .HasOne(cs => cs.ChannelSettings)
                .WithMany(cs => cs.ChannelsGlobalEvents)
                .HasForeignKey(cge => cge.Channel)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<ChannelsGlobalEvents>()
                .HasOne(e => e.Event)
                .WithMany(e => e.ChannelsGlobalEvents)
                .HasForeignKey(cge => cge.Id)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);


            modelBuilder.Entity<Moderator>().HasKey(m => m.Id);

            modelBuilder.Entity<Moderator>()
                .HasOne(cs => cs.Streamer)
                .WithMany(user => user.ChannelModerators)
                .HasForeignKey(moderator => moderator.Channel)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Moderator>()
                .HasOne(cs => cs.Mod)
                .WithMany(user => user.ModeratedChannels)
                .HasForeignKey(moderator => moderator.ModName)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite(@"Data Source=.\LocalStorage\omnisciencebot.db");
    }
}