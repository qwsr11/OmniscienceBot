﻿using Microsoft.EntityFrameworkCore;
using OmniscienceBotCore.DataManagement;
using OmniscienceBotCore.Models.Events;
using OmniscienceBotCore.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Channel
{
    public class GlobalEvents : ChannelCollection<Event, int>
    {
        public string CurrentKey { get; set; }


        TwitchClientWrapper twitchClient;
        public GlobalEvents(TwitchClientWrapper _client) : base("")
        {
            twitchClient = _client;

            Init();
        }

        protected override List<Event> GetCollection(DataContext db, string channel) => db.Events.Include(e => e.ChannelsGlobalEvents).AsQueryable().Where(item => item.Channel == "").ToList();
        protected override Event Find(DataContext db, int id) => db.Events.Find(id);
        void Init()
        {
            foreach (var item in Collection)
                item.Init(twitchClient);
        }

        public void Add(Event item, List<string> channels, string issuedChannel)
        {
            using (DataContext db = new DataContext())
            {
                db.Add(item);
                db.SaveChanges();

                foreach (var channel in channels)
                    db.Add(new ChannelsGlobalEvents() { Channel = channel, Id = item.Id, Subscribed = issuedChannel == channel ? 1 : 0 });

                db.SaveChanges();

                Collection.Add(item);
            }
        }

        public new void Change(Event item)
        {
            using (DataContext db = new DataContext())
            {
                Event e = db.Events.Include(e => e.ChannelsGlobalEvents).ThenInclude(e => e.Event).First(e => e.Id == item.Id);
                e.Json = item.Json;
                e.Active = item.Active;
                e.Init(twitchClient);
                db.Events.Update(e);
                db.SaveChanges();

                Collection[Collection.FindIndex(p => p.Id == item.Id)] = e;
            }
        }

        public void Sub(string channel, int id)
        {
            using (DataContext db = new DataContext())
            {
                var cge = Collection[Collection.FindIndex(p => p.Id == id)].ChannelsGlobalEvents.First(cge => cge.Channel == channel);
                cge.Subscribed = 1;
                db.Entry(cge).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void Unsub(string channel, int id)
        {
            using (DataContext db = new DataContext())
            {
                var cge = Collection[Collection.FindIndex(p => p.Id == id)].ChannelsGlobalEvents.First(cge => cge.Channel == channel);
                cge.Subscribed = 0;
                db.Entry(cge).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
    }
}