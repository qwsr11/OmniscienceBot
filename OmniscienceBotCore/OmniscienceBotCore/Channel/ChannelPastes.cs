﻿using OmniscienceBotCore.DataManagement;
using OmniscienceBotCore.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace OmniscienceBotCore.Channel
{
    [NotMapped]
    public class ChannelPastes : ChannelCollection<Paste, int>
    {
        public ChannelPastes(string channel) : base(channel)
        {
        }

        protected override Paste Find(DataContext db, int id) => db.Pastes.Find(id);
        protected override List<Paste> GetCollection(DataContext db, string channel) => db.Pastes.AsQueryable().Where(c => c.Channel == channel).ToList();
    }
}