﻿using OmniscienceBotCore.DataManagement;
using OmniscienceBotCore.Exceptions;
using OmniscienceBotCore.Models;
using OmniscienceBotCore.Models.Commands;
using OmniscienceBotCore.Models.Events;
using OmniscienceBotCore.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TwitchLib.Client.Models;
using TwitchLib.PubSub.Events;
using Stream = TwitchLib.Api.Helix.Models.Streams.GetStreams.Stream;
using Discord.WebSocket;
using Discord;
using Microsoft.Extensions.Logging;
using TwitchLib.Api;
using Newtonsoft.Json;

namespace OmniscienceBotCore.Channel
{
    public class TwitchChannels
    {
        TwitchClientWrapper twitchClient;
        TwitchAPI twitchAPI;
        DiscordWrapper discord;
        private readonly ILogger logger;

        public Dictionary<string, TwitchChannel> Channels { get; set; }
        public GlobalEvents GlobalEvents { get; set; }

        public TwitchChannel this[string channel]
        {
            get
            {
                if (Channels.ContainsKey(channel)) return Channels[channel];
                else throw new ChannelNotFoundException(channel);
            }
        }

        public TwitchChannels(DiscordWrapper discord, TwitchAPI twitchAPI, ILogger logger)
        {
            this.discord = discord;
            this.twitchAPI = twitchAPI;
            this.logger = logger;
            discord.MessageReceived += Client_MessageReceived;
        }

        private async Task Client_MessageReceived(SocketMessage rawMessage)
        {
            if (!(rawMessage is IUserMessage message)) return;
            if (message.Source != MessageSource.User) return;

            var channel = message.Channel as SocketGuildChannel;
            string guildName = channel.Guild.Name;

            var twitchChannel = Channels.Values.FirstOrDefault(tc => tc.ChannelSettings.DiscordServerName == guildName);
            if (twitchChannel != null)
            {
                string response = twitchChannel.ProcessDiscordCommands(message.Channel.Name, message.Content);
                if (response != null && response != "") await message.Channel.SendMessageAsync(discord.ProcessEmotes(response));
            }
        }

        public void InitChannels(TwitchClientWrapper twitchClientWrapper)
        {
            this.twitchClient = twitchClientWrapper;
            GlobalEvents = new GlobalEvents(twitchClientWrapper);

            Channels = new Dictionary<string, TwitchChannel>();

            using (DataContext db = new DataContext())
            {
                var data = db.ChannelSettings.ToList();
                if (data == null) return;

                for (int i = 0; i < data.Count; i++)
                {
                    SettingsMain settings = JsonConvert.DeserializeObject<SettingsMain>(data[i].RawJson, new JsonSerializerSettings() { MetadataPropertyHandling = MetadataPropertyHandling.Ignore });
                    string channel = settings.Channel;

                    var user = twitchAPI.Helix.Users.GetUsersAsync(logins: new List<string>() { channel }).Result.Users.First();
                    string ChannelID = user.Id;
                    string ProfileImage = user.ProfileImageUrl;

                    TwitchChannel twitchChannel = new TwitchChannel(settings,
                                                                    this.twitchClient,
                                                                    ChannelID,
                                                                    ProfileImage);

                    Channels.Add(channel, twitchChannel);
                }
            }
        }

        public void OnStreamUp(object sender, OnStreamUpArgs e)
        {
            TwitchChannel channel = Channels.Values.First(c => c.ChannelID == e.ChannelId);
            var time = DateTime.UtcNow;
            channel.StreamUpTime = time;
            channel.StreamStarted = time;

            ProcessAnnouncements(channel.ChannelSettings.Channel, time, true);
        }

        public void OnStreamDown(object sender, OnStreamDownArgs e)
        {
            TwitchChannel channel = Channels.Values.First(c => c.ChannelID == e.ChannelId);
            var time = DateTime.UtcNow;
            channel.StreamUpTime = null;
            channel.StreamStopped = time;
            foreach (Counter counter in channel.Counters.Collection)
                counter.Count = 0;

            ProcessAnnouncements(channel.ChannelSettings.Channel, time, false);
        }

        public void OnPrediction(object sender, OnPredictionArgs e)
        {
            TwitchChannel channel = Channels.Values.First(c => c.ChannelID == e.ChannelId);

            if (channel != null)
                Channels[channel.ChannelSettings.Channel].ProcessPredictions(channel.ChannelSettings.Channel, e);
        }

        //public void OnRewardRedeemed(object sender, OnChannelPointsRewardRedeemedArgs e)
        //{
        //    TwitchChannel channel = Channels.Values.First(c => c.ChannelID == e.ChannelId);

        //    if (channel != null) Channels[channel.ChannelSettings.Channel].ProcessRewards(channel.ChannelSettings.Channel, e);
        //}

        public void OnRewardRedeemed(object sender, OnRewardRedeemedArgs e)
        {
            if (e.Status == "UNFULFILLED")
            {
                TwitchChannel channel = Channels.Values.First(c => c.ChannelID == e.ChannelId);

                if (channel != null) Channels[channel.ChannelSettings.Channel].ProcessRewards(channel.ChannelSettings.Channel, e);
            }
        }

        public void Monitor_OnStreamOnline(object sender, TwitchLib.Api.Services.Events.LiveStreamMonitor.OnStreamOnlineArgs e)
        {
            TwitchChannel channel = Channels.Values.First(c => c.ChannelID == e.Channel);
            var time = DateTime.UtcNow;
            channel.StreamUpTime = time;
            channel.StreamStarted = time;

            if (time.Subtract(e.Stream.StartedAt).TotalMinutes < 5)
                ProcessAnnouncements(channel.ChannelSettings.Channel, time, true);
        }

        public void Monitor_OnStreamOffline(object sender, TwitchLib.Api.Services.Events.LiveStreamMonitor.OnStreamOfflineArgs e)
        {
            TwitchChannel channel = Channels.Values.First(c => c.ChannelID == e.Channel);
            var time = DateTime.UtcNow;
            channel.StreamUpTime = null;
            channel.StreamStopped = time;
            foreach (Counter counter in channel.Counters.Collection)
                counter.Count = 0;

            ProcessAnnouncements(channel.ChannelSettings.Channel, time, false);
        }

        async void ProcessAnnouncements(string channelName, DateTime time, bool up)
        {
            Stream stream = null;
            do
            {
                stream = (await twitchAPI.Helix.Streams.GetStreamsAsync(userLogins: new List<string>() { channelName })).Streams.FirstOrDefault();

                if (stream == null) await Task.Delay(TimeSpan.FromSeconds(30));
            } while (stream == null);

            foreach (TwitchChannel channel in Channels.Values)
                channel.ProcessAnnouncements(discord, channelName, stream, Channels[channelName].ProfileImageURL, time, up);
        }

        private void TwitchPubSub_OnFollow(object sender, OnFollowArgs e)
        {
            TwitchChannel channel = Channels.Values.First(c => c.ChannelID == e.FollowedChannelId);
            if (channel.ChannelSettings.FollowReactionFormat != null && channel.ChannelSettings.FollowReactionFormat != "")
                twitchClient.SendMessage(e.FollowedChannelId, channel.ChannelSettings.FollowReactionFormat.Replace("@source", e.DisplayName));
        }

        public void AddChannel(string channel, bool isStreamer = false)
        {
            using (DataContext db = new DataContext())
            {
                if (db.ChannelSettings.Find(channel) != null) return;
                var user = twitchAPI.Helix.Users.GetUsersAsync(logins: new List<string>() { channel }).Result.Users.First();
                string ChannelID = user.Id;
                string ProfileImage = user.ProfileImageUrl;
                TwitchChannel twitchChannel = new TwitchChannel(
                    new SettingsMain() { Channel = channel, Active = true, IsStreamer = isStreamer, SilenceMode = true },
                    twitchClient,
                    ChannelID,
                    ProfileImage);

                foreach (var item in GlobalEvents.Collection)
                    db.Add(new ChannelsGlobalEvents() { Channel = channel, Id = item.Id, Subscribed = 0 });

                db.Add(twitchChannel.ChannelSettings);
                db.SaveChanges();
                GlobalEvents = new GlobalEvents(twitchClient);
                Channels.Add(channel, twitchChannel);
            }
        }

        public void ProcessEvents(ChatMessage chatMessage, TwitchChannelRole role)
        {
            foreach (Event e in GlobalEvents.Collection.Where(e => e.Active && e.ChannelsGlobalEvents.ToList().Exists(c => c.Channel == chatMessage.Channel && c.Subscribed == 1)))
                e.Execute(chatMessage, role);
        }

        public ChannelCommands GetChannelCommands(string channel)
        {
            return this[channel].Commands;
        }
    }
}