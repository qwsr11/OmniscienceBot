﻿using Microsoft.EntityFrameworkCore;
using NuGet.Protocol;
using OmniscienceBotCore.DataManagement;
using OmniscienceBotCore.Models;
using OmniscienceBotCore.Models.Commands;
using OmniscienceBotCore.Models.Events;
using OmniscienceBotCore.Services;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading;
using TwitchLib.Api.Helix.Models.Streams.GetStreams;
using TwitchLib.Client.Models;

namespace OmniscienceBotCore.Channel
{
    [NotMapped]
    public class TwitchChannel
    {
        SettingsMain settings;
        public SettingsMain ChannelSettings
        {
            get => settings;
            set
            {
                settings = value;

                using (DataContext db = new DataContext())
                {
                    db.Entry(new ChannelSettings(settings.Channel, settings.ToJson())).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }

        #region base params
        public string ChannelID { get; private set; }
        public string ProfileImageURL { get; private set; }

        #region Active and spinwait for commands
        public int MessagesBefore { get; set; }

        Timer CommandLoopTimer;
        public void LoopStart() => CommandLoopTimer = new Timer(CommandProcessingLoop, Environment.TickCount, 0, 1000);
        public void LoopAbort() => CommandLoopTimer = null;
        #endregion
        #endregion

        #region collections
        [ManyToMany(typeof(ChannelsGlobalEvents))]
        public List<Event> GlobalEvents { get; set; }
        public ChannelCommands Commands { get; private set; }
        public ChannelPastes Pastes { get; private set; }
        public ChannelEvents Events { get; private set; }
        public ChannelCounters Counters { get; private set; }
        public StreamAnnouncements Announcements { get; private set; }
        public List<string> Trustworthy { get; private set; }
        public ChannelModerators Moderators { get; private set; }
        #endregion

        #region songs
        public Queue<string> ToPlay { get; set; }
        public Queue<Song> SongRequestQueue { get; set; }
        public bool QueueIsEmpty { get => ToPlay.Count == 0; }

        public string CheckForQueue => ToPlay.Count > 0 ? ToPlay.Dequeue() : null;
        public Song SongRequestCheck => SongRequestQueue.Count > 0 ? SongRequestQueue.Dequeue() : null;
        #endregion

        #region online
        public bool Online => StreamStopped - StreamStarted < TimeSpan.Zero;

        public DateTime StreamStarted { get; set; }
        public DateTime StreamStopped { get; set; }

        public int? StreamUp => StreamUpTime == null ? (int?)null : (DateTime.UtcNow - StreamUpTime).Value.Hours;
        public DateTime? StreamUpTime { get; set; }

        public void ForceOnline() => StreamUpTime = DateTime.UtcNow;
        #endregion

        #region Clients and services
        TwitchClientWrapper TwitchClient;
        public SpotifyWrapper Spotify;
        #endregion

        public TwitchChannel(SettingsMain settings, TwitchClientWrapper twitchClient, string channelID, string profileImageURL)
        {
            this.ChannelSettings = settings;
            TwitchClient = twitchClient;
            ChannelID = channelID;
            ProfileImageURL = profileImageURL;

            MessagesBefore = 0;

            ToPlay = new Queue<string>();
            SongRequestQueue = new Queue<Song>();


            Commands = new ChannelCommands(settings.Channel);
            Pastes = new ChannelPastes(settings.Channel);
            Events = new ChannelEvents(settings.Channel, TwitchClient);
            Counters = new ChannelCounters(settings.Channel);
            Announcements = new StreamAnnouncements(settings.Channel);

            Moderators = new ChannelModerators(settings.Channel);

            Spotify = new SpotifyWrapper(TwitchClient.config, settings.SpotifyRefreshToken);

            TrustworthyInit();
        }

        Queue<TextCommand> commandsQueue = new Queue<TextCommand>();
        void CommandProcessingLoop(object state)
        {
            foreach (TextCommand command in Commands.Collection.Where(c =>
                        (c is TextCommand tc) &&
                        (tc.CommandHandler == CommandHandler.Stream || tc.CommandHandler == CommandHandler.Both) &&
                        tc.Periodic &&
                        (DateTime.Now.Subtract(tc.LastPeriodicExecute).TotalSeconds > tc.Period)))
                if (!commandsQueue.Contains(command))
                    commandsQueue.Enqueue(command);

            TryToDequeue();
        }

        bool TryToDequeue()
        {
            if (commandsQueue.Count == 0 || MessagesBefore < 2) return false;

            TwitchClient.SendMessage(ChannelSettings.Channel, commandsQueue.Dequeue().Execute(TwitchChannelRole.Broadcaster).ToString());
            MessagesBefore = 0;
            return true;
        }

        public string ProcessDiscordCommands(string channel, string message)
        {
            if (message.StartsWith('!') && message.Length > 1)
            {
                string key = message[1..];

                TextCommand command = Commands.Collection.FirstOrDefault(c => c is TextCommand cmd
                                                                                    && (cmd.Key.Contains(',') ? cmd.Key.Split(',').Contains(key) : cmd.Key == key)
                                                                                    && (cmd.CommandHandler == CommandHandler.Discord || cmd.CommandHandler == CommandHandler.Both)) as TextCommand;
                return (string)command?.Execute(channel);
            }

            return null;
        }

        public void ProcessEvents(ChatMessage chatMessage, TwitchChannelRole role)
        {
            foreach (Event e in Events.Collection.Where(e => e.Active))
                e.Execute(chatMessage, role);
        }

        public void ProcessCounters(ChatMessage chatMessage, TwitchChannelRole role)
        {
            foreach (Counter counter in Counters.Collection)
                counter.Execute(TwitchClient, chatMessage, role);
        }

        public void ProcessAnnouncements(DiscordWrapper discord, string channelName, Stream stream, string profileImageUrl, DateTime time, bool up)
        {
            var announcement = Announcements.Collection.FirstOrDefault(a => a.StreamToAnnounce == channelName);
            if (announcement != null)
                if (up)
                {
                    string msg = announcement.Execute(time);
                    if (msg == null) return;

                    discord.SendAnnouncementAsync(ChannelSettings.DiscordServerName, announcement.DiscordChannel, msg, stream, profileImageUrl);
                }
                else announcement.Stop(time);
        }

        public void ProcessPredictions(string channelName, TwitchLib.PubSub.Events.OnPredictionArgs prediction)
        {
            if (prediction.Type == TwitchLib.PubSub.Enums.PredictionType.EventCreated &&
                !string.IsNullOrEmpty(ChannelSettings.PredictionStartFormat))
                TwitchClient.SendMessage(channelName, MarkupSubstitution(ChannelSettings.PredictionStartFormat, prediction));

            if (prediction.Type == TwitchLib.PubSub.Enums.PredictionType.EventUpdated &&
                prediction.Status == TwitchLib.PubSub.Enums.PredictionStatus.Canceled &&
                !string.IsNullOrEmpty(ChannelSettings.PredictionCancelFormat))
                TwitchClient.SendMessage(channelName, MarkupSubstitution(ChannelSettings.PredictionCancelFormat, prediction));
        }

        protected static string MarkupSubstitution(string reactionFormat, TwitchLib.PubSub.Events.OnPredictionArgs prediction)
        {
            reactionFormat = reactionFormat.Replace("@title", prediction.Title);
            reactionFormat = reactionFormat.Replace("@totalUsers", prediction.Outcomes.Select(p => p.TotalUsers).Sum().ToString());
            reactionFormat = reactionFormat.Replace("@totalPoints", prediction.Outcomes.Select(p => p.TotalPoints).Sum().ToString());

            if (prediction.Status == TwitchLib.PubSub.Enums.PredictionStatus.Resolved)
            {
                TwitchLib.PubSub.Models.Outcome winningOutcome = prediction.Outcomes.First(p => p.Id == prediction.WinningOutcomeId);
                reactionFormat = reactionFormat.Replace("@wonTitle", winningOutcome.Title);
                reactionFormat = reactionFormat.Replace("@wonTotalUsers", winningOutcome.TotalPoints.ToString());
                reactionFormat = reactionFormat.Replace("@wonTotalPoints", winningOutcome.TotalPoints.ToString());
            }

            return reactionFormat;
        }


        public async void ProcessRewards(string channelName, TwitchLib.PubSub.Events.OnRewardRedeemedArgs reward)
        {
            TwitchChannelRole role = TwitchChannelRole.Moderator;
            foreach (Event e in Events.Collection.Where(e => e.Active))
                e.Execute(reward, channelName, role);
        }

        void TrustworthyInit()
        {
            Trustworthy = new List<string>();
        }

        public void SpotifyInit(string refreshToken, string accessToken)
        {
            Spotify.Init(refreshToken, accessToken);
            if (ChannelSettings.SpotifyRefreshToken != refreshToken)
            {
                var t = ChannelSettings;
                t.SpotifyRefreshToken = refreshToken;
                ChannelSettings = t;
            }
        }

        public void CancelFollowerMode(int viewersOnRaid)
        {
            if (ChannelSettings.CancelFollowerModeOnRaid != 0 &&
                viewersOnRaid >= ChannelSettings.CancelFollowerModeOnRaid)
                TwitchClient.SetFollowerOnly(ChannelID);
        }

        public void SendShoutout(string raidFromId, int viewersOnRaid)
        {
            if (ChannelSettings.ShoutoutOnRaid != 0 &&
                viewersOnRaid >= ChannelSettings.ShoutoutOnRaid)
                TwitchClient.SendShoutout(ChannelID, raidFromId);
        }
    }
}