﻿using Newtonsoft.Json;
using OmniscienceBotCore.DataManagement;
using OmniscienceBotCore.Models;
using OmniscienceBotCore.Models.Commands;
using OmniscienceBotCore.Models.Events;
using OmniscienceBotCore.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace OmniscienceBotCore.Channel
{
    [NotMapped]
    public class ChannelEvents : ChannelCollection<Event, int>
    {
        public int CurrentKey { get; set; } = -1;

        TwitchClientWrapper twitchClient;
        public ChannelEvents(string channel, TwitchClientWrapper client) : base(channel)
        {
            twitchClient = client;
            Init();
        }

        protected override List<Event> GetCollection(DataContext db, string channel) => db.Events.AsQueryable().Where(item => item.Channel == channel).ToList().Where(item => !item.IsPublic).ToList();
        protected override Event Find(DataContext db, int id) => db.Events.Find(id);

        void Init()
        {
            foreach (var item in Collection)
                item.Init(twitchClient);
        }

        public new void Change(Event item, string editor)
        {
            using (DataContext db = new DataContext())
            {
                Event e = db.Find<Event>(item.Id);
                e.Json = item.Json;
                e.Active = item.Active;
                e.Init(twitchClient);
                db.Events.Update(e);
                db.SaveChanges();

                db.Add(new EntityChange(Channel, editor ?? Channel, DateTime.UtcNow.ToString(), item.GetType().Name, item.Id.ToString(), JsonConvert.SerializeObject(item)));
                db.SaveChanges();

                Collection[Collection.FindIndex(p => p.Id == item.Id)] = e;
            }
        }
    }
}