﻿using Newtonsoft.Json.Linq;
using OmniscienceBotCore.DataManagement;
using OmniscienceBotCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OmniscienceBotCore.Channel
{
    public class ChannelModerators : ChannelCollection<Moderator, int>
    {
        public ChannelModerators(string channel) : base(channel) { }

        protected override Moderator Find(DataContext db, int id) =>
            db.ChannelModerators.Find(id);

        protected override List<Moderator> GetCollection(DataContext db, string channel) =>
            db.ChannelModerators.AsQueryable()
            .Where(item => item.Channel == channel)
        .ToList();

        public Moderator this[string modName] => Collection.FirstOrDefault(item => item.ModName == modName);
    }
}