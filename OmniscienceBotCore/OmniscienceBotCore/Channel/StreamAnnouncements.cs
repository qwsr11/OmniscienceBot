﻿using Microsoft.EntityFrameworkCore;
using OmniscienceBotCore.DataManagement;
using OmniscienceBotCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Channel
{
    public class StreamAnnouncements : ChannelCollection<StreamAnnouncement, int>
    {
        public StreamAnnouncements(string channel):base(channel)
        {
        }

        protected override StreamAnnouncement Find(DataContext db, int id) => db.StreamAnnouncements.Find(id);
        protected override List<StreamAnnouncement> GetCollection(DataContext db, string channel) => db.StreamAnnouncements.AsQueryable().Where(c => c.Channel == channel).ToList();
    }
}
