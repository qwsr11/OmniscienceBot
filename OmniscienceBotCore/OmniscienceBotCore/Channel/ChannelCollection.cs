﻿using Microsoft.EntityFrameworkCore;
using NuGet.Protocol;
using OmniscienceBotCore.DataManagement;
using OmniscienceBotCore.Models;
using OmniscienceBotCore.Models.Commands;
using OmniscienceBotCore.Models.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace OmniscienceBotCore.Channel
{
    public abstract class ChannelCollection<Value, idType> where Value : IdModel<idType>
    {
        public List<Value> Collection { get; set; }
        public string Channel { get; }

        protected ChannelCollection(string channel)
        {
            using (DataContext db = new DataContext())
                Collection = GetCollection(db, channel);
            Channel = channel;
        }

        protected abstract List<Value> GetCollection(DataContext db, string channel);

        protected abstract Value Find(DataContext db, idType id);

        public void Add(Value item, string editor = null)
        {
            using (DataContext db = new DataContext())
            {
                if (Find(db, item.Id) == null)
                {
                    db.Add(item);
                    db.SaveChanges();

                    db.Add(new EntityChange(Channel, editor ?? Channel, DateTime.UtcNow.ToString(), item.GetType().Name, item.Id.ToString(), JsonConvert.SerializeObject(item)));
                    db.SaveChanges();

                    Collection.Add(item);
                }
            }
        }

        public void Change(Value item, string editor = null)
        {
            using (DataContext db = new DataContext())
            {
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();

                db.Add(new EntityChange(Channel, editor ?? Channel, DateTime.UtcNow.ToString(), item.GetType().Name, item.Id.ToString(), JsonConvert.SerializeObject(item)));
                db.SaveChanges();

                Collection[Collection.FindIndex(p => p.Id.Equals(item.Id))] = item;
            }
        }

        public void Delete(idType id, string editor = null)
        {
            using (DataContext db = new DataContext())
            {
                Value item = Find(db, id);
                if (item != null)
                {
                    db.Remove(item);
                    db.SaveChanges();

                    db.Add(new EntityChange(Channel, editor ?? Channel, DateTime.UtcNow.ToString(), item.GetType().Name, item.Id.ToString(), "Removed."));
                    db.SaveChanges();

                    Collection.Remove(item);
                }
            }
        }

        public Value this[idType id] => Collection.FirstOrDefault(item => item.Id.Equals(id));
        public bool Exists(idType id) => Collection.Exists(item => item.Id.Equals(id));
    }
}