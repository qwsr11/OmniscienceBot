﻿using OmniscienceBotCore.DataManagement;
using OmniscienceBotCore.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace OmniscienceBotCore.Channel
{
    [NotMapped]
    public class ChannelCounters : ChannelCollection<Counter, int>
    {
        public ChannelCounters(string channel) : base(channel)
        {
        }

        protected override Counter Find(DataContext db, int id) => db.Counters.Find(id);

        protected override List<Counter> GetCollection(DataContext db, string channel) => db.Counters.AsQueryable().Where(c => c.Channel == channel).ToList();
    }
}
