﻿using Microsoft.EntityFrameworkCore;
using OmniscienceBotCore.DataManagement;
using OmniscienceBotCore.Models.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace OmniscienceBotCore.Channel
{
    [NotMapped]
    public class ChannelCommands : ChannelCollection<Command, int>
    {
        public ChannelCommands(string channel) : base(channel)
        {
        }

        protected override List<Command> GetCollection(DataContext db, string channel) => db.Commands.AsQueryable().Where(c => c.Channel == channel).ToList();
        protected override Command Find(DataContext db, int id) => db.Commands.Find(id);

        public bool Exists(string key) => Collection.Exists(item => item.Key.Contains(',') ? item.Key.Split(',').Contains(key) : item.Key == key);
        public Command this[string key] => Collection.FirstOrDefault(item => item.Key.Contains(',') ? item.Key.Split(',').Contains(key) : item.Key == key);
    }
}
