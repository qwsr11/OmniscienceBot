﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using OmniscienceBotCore.Channel;
using OmniscienceBotCore.Models;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TwitchLib.PubSub;
using TwitchLib.PubSub.Events;

namespace OmniscienceBotCore.Services
{
    //https://github.com/JayJay1989/TwitchLib.Pubsub.Example/blob/main/ExampleTwitchPubsub/Program.cs
    public class TwitchPubSubWrapper
    {
        TwitchPubSub client;
        private readonly ILogger logger;
        string oauth;

        public TwitchPubSubWrapper(TwitchChannels channels, ILogger logger, ILogger<TwitchPubSub> innerLogger)
        {
            this.logger = logger;
            client = new TwitchPubSub();

            //foreach (TwitchChannel channel in channels.Channels.Values)
            //    Playback_AddChannelToListen(channel.ChannelID);

            foreach (TwitchChannel channel in channels.Channels.Values)
            {
                Predictions_AddChannelToListen(channel.ChannelID);
                Rewards_AddChannelToListen(channel.ChannelID);
                //Points_AddChannelToListen(channel.ChannelID);
            }

            client.OnPubSubServiceError += Client_OnPubSubServiceError;
            client.OnPubSubServiceConnected += TwitchPubSub_OnPubSubServiceConnected;
            //client.OnStreamUp += channels.OnStreamUp;
            //client.OnStreamDown += channels.OnStreamDown;
            client.OnPrediction += channels.OnPrediction;
            //client.OnChannelPointsRewardRedeemed += channels.OnRewardRedeemed;
            client.OnRewardRedeemed += channels.OnRewardRedeemed;


            client.Connect();
        }

        public void Playback_AddChannelToListen(string channelID) => client.ListenToVideoPlayback(channelID);
        public void Predictions_AddChannelToListen(string channelID) => client.ListenToPredictions(channelID);
        public void Rewards_AddChannelToListen(string channelID) => client.ListenToRewards(channelID);
        public void Points_AddChannelToListen(string channelID) => client.ListenToChannelPoints(channelID);

        public void Raid_AddChannelToListen(string channelID) => client.ListenToRaid(channelID);

        private void Client_OnPubSubServiceError(object sender, OnPubSubServiceErrorArgs e) => logger.LogError(e.Exception, e.Exception.Message);

        private void TwitchPubSub_OnPubSubServiceConnected(object sender, EventArgs e) => client.SendTopics(oauth);
    }
}
