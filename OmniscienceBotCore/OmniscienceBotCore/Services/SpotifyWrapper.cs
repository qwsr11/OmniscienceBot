﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Options;
using OmniscienceBotCore.DataManagement;
using SpotifyAPI.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static OmniscienceBotCore.Startup;

namespace OmniscienceBotCore.Services
{
    public class SpotifyWrapper
    {
        public class SpotfyResponse
        {
            public string Status { get; set; }
            public string Message { get; set; }
        }

        SpotifyClient client;
        string clientID;
        string clientSecret;
        string refreshToken;

        async Task<string> Precheck()
        {
            if (ClientUnauthorized) return "Клиент Spotify не прошел авторизацию. Загляните на сайт бота.";
            if (!(await client.Player.GetAvailableDevices()).Devices.Any(d => d.Type.ToLower() == "computer")) return "Не обнаружено устройств для воспроизведения. Попросите нажать ручками кнопочку плей. Только ВЕЖЛИВО, а то стримлер малость нервный Kappa";
            return "OK";

        }
        bool ClientUnauthorized { get => client == null; }

        public string RefreshToken { get => refreshToken; private set => refreshToken = value; }

        public SpotifyWrapper(IOptions<AppConfig> config, string refreshToken = "")
        {
            clientID = config.Value.SpotifyClientId;
            clientSecret = config.Value.SpotifyClientSecret;
            if (refreshToken != "")
            {
                this.RefreshToken = refreshToken;
                Init(RefreshToken);
            }
        }

        public void Init(string refreshToken, string accessToken = "")
        {
            if (RefreshToken != refreshToken) RefreshToken = refreshToken;
            if (accessToken != "") client = new SpotifyClient(accessToken);
            else UpdateToken();
        }

        void UpdateToken()
        {
            if (RefreshToken == null || RefreshToken == "") return;
            var response = new OAuthClient().RequestToken(new AuthorizationCodeRefreshRequest(clientID, clientSecret, RefreshToken)).Result;

            client = new SpotifyClient(response.AccessToken);
        }
        public async Task<SpotfyResponse> Song()
        {
            try
            {
                string status = await Precheck();
                if (status != "OK") return new SpotfyResponse() { Status = "API error", Message = status };

                var t = (await client.Player.GetCurrentlyPlaying(new PlayerCurrentlyPlayingRequest())).Item as FullTrack;

                return new SpotfyResponse() { Status = "OK", Message = $"Трек: {t.Artists.First().Name} - {t.Name} (open.spotify.com/track/{t.Id})" };
            }
            catch (Exception e)
            {
                if (e.Message.Contains("The access token expired"))
                {
                    UpdateToken();
                    return await Song();
                }

                return new SpotfyResponse() { Status = "API error", Message = e.Message };
            }
        }

        public async Task<SpotfyResponse> Play()
        {
            try
            {
                string status = await Precheck();
                if (status != "OK") return new SpotfyResponse() { Status = "API error", Message = status };

                var currentPlayback = await client.Player.GetCurrentPlayback();
                if (currentPlayback != null && !currentPlayback.IsPlaying)
                    return new SpotfyResponse()
                    {
                        Status = "OK",
                        Message = await client.Player.ResumePlayback() ? "Resumed." : "Failed to resume."
                    };


                return new SpotfyResponse() { Status = "OK", Message = "" };
            }
            catch (Exception e)
            {
                if (e.Message.Contains("The access token expired"))
                {
                    UpdateToken();
                    return await Play();
                }

                return new SpotfyResponse() { Status = "API error", Message = e.Message };
            }
        }

        public async Task<SpotfyResponse> Pause()
        {
            try
            {
                string status = await Precheck();
                if (status != "OK") return new SpotfyResponse() { Status = "API error", Message = status };

                var currentPlayback = await client.Player.GetCurrentPlayback();
                if (currentPlayback != null && currentPlayback.IsPlaying)
                    return new SpotfyResponse()
                    {
                        Status = "OK",
                        Message = await client.Player.PausePlayback() ? "Paused." : "Failed to pause."
                    };

                return new SpotfyResponse() { Status = "OK", Message = "" };
            }
            catch (Exception e)
            {
                if (e.Message.Contains("The access token expired"))
                {
                    UpdateToken();
                    return await Pause();
                }

                return new SpotfyResponse() { Status = "API error", Message = e.Message };
            }
        }

        public async Task<SpotfyResponse> Skip()
        {
            try
            {
                string status = await Precheck();
                if (status != "OK") return new SpotfyResponse() { Status = "API error", Message = status };

                var skipRes = await Song();
                if (skipRes.Status != "OK") return skipRes;

                bool res = await client.Player.SkipNext();

                return new SpotfyResponse() { Status = "OK", Message = res ? $"Skipped: {skipRes.Message}" : "Failed to skip." }; ;
            }
            catch (Exception e)
            {
                if (e.Message.Contains("The access token expired"))
                {
                    UpdateToken();
                    return await Skip();
                }

                return new SpotfyResponse() { Status = "API error", Message = e.Message };
            }
        }

        public async Task<SpotfyResponse> Add(string uri)
        {
            try
            {
                string status = await Precheck();
                if (status != "OK") return new SpotfyResponse() { Status = "API error", Message = status };

                bool res = await client.Player.AddToQueue(new PlayerAddToQueueRequest(uri));

                var songResponse = client.Tracks.Get(uri.Split(':').Last()).Result;

                return new SpotfyResponse() { Status = "OK", Message = res ? $"Added: {songResponse.Artists.First().Name} - {songResponse.Name}" : "Failed to add." }; ;
            }
            catch (Exception e)
            {
                if (e.Message.Contains("The access token expired"))
                {
                    UpdateToken();
                    return await Add(uri);
                }

                return new SpotfyResponse() { Status = "API error", Message = e.Message };
            }
        }

        //public SpotfyResponse List()
        //{
        //    try
        //    {
        //        string status = Precheck;
        //        if (status != "OK") return new SpotfyResponse() { Status = "API error", Message = status };

        //        bool res = spotify..Result;

        //        return new SpotfyResponse() { Status = "OK", Message = res ? "Skipped." : "Failed to skip." }; ;
        //    }
        //    catch (Exception e)
        //    {
        //        if (e.Message.Contains("The access token expired"))
        //        {
        //            UpdateToken();
        //            return Skip();
        //        }

        //        return new SpotfyResponse() { Status = "API error", Message = e.Message };
        //    }
        //}
    }
}