﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OmniscienceBotCore.Channel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using TwitchLib.Api;
using TwitchLib.Api.Helix.Models.Streams.GetStreams;
using TwitchLib.Api.Helix.Models.Users.GetUsers;
using static OmniscienceBotCore.Startup;

namespace OmniscienceBotCore.Services
{
    public class DiscordWrapper : BackgroundService
    {
        string token;
        readonly DiscordSocketClient discordClient = new DiscordSocketClient();
        private readonly ILogger logger;

        public event Func<SocketMessage, Task> MessageReceived
        {
            add => discordClient.MessageReceived += value;
            remove => discordClient.MessageReceived -= value;
        }

        public DiscordWrapper(IOptions<AppConfig> options, ILogger logger)
        {
            this.logger = logger;
            this.token = options.Value.DiscordToken;

            Init();
        }

        async void Init()
        {
            discordClient.Log += DiscordClient_Log;
            await discordClient.LoginAsync(TokenType.Bot, token);
            await discordClient.StartAsync();
        }

        private Task DiscordClient_Log(LogMessage arg)
        {
            logger.LogInformation(arg.Message);
            return null;
        }

        public string ProcessEmotes(string message)
        {
            return Regex.Replace(message, ":\\w+?:", m =>
            {
                var emote = discordClient.Guilds.SelectMany(g => g.Emotes).FirstOrDefault(e => e.Name == m.Value[1..^1]);
                return emote == null ? m.Value : $"<:{emote.Name}:{emote.Id}>";
            });
        }

        public async void SendAnnouncementAsync(string guildName, string guildChannelName, string message, Stream stream, string ProfileImageUrl)
        {
            message = ProcessEmotes(message);

            var guild = discordClient.Guilds.First(g => g.Name == guildName);
            var channel = guild.Channels.First(c => c.Name == guildChannelName);

            var builder = new EmbedBuilder();
            builder
    .WithTitle(stream.Title)
    .WithAuthor(stream.UserName, $"{ProfileImageUrl}")
    .WithUrl($"https://www.twitch.tv/{stream.UserLogin}")
    .WithImageUrl(stream.ThumbnailUrl.Replace("{width}", "1280").Replace("{height}", "720") + "?r=" + new Random().Next((int)10e8).ToString())
    .WithAuthor(stream.UserName, url: $"https://www.twitch.tv/{stream.UserLogin}")
    .WithThumbnailUrl($"{ProfileImageUrl}");

            await discordClient.GetGuild(guild.Id).GetTextChannel(channel.Id).SendMessageAsync(message, embed: builder.Build());
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                while (!stoppingToken.IsCancellationRequested)
                    await Task.Delay(1000);
            }
            catch (OperationCanceledException e) { Console.WriteLine(e.Message); }
        }
    }
}