﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OmniscienceBotCore.Channel;
using OmniscienceBotCore.Models;
using OmniscienceBotCore.Models.Commands;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using TwitchLib.Api;
using TwitchLib.Api.Helix.Models.Chat;
using TwitchLib.Api.Helix.Models.Moderation.BanUser;
using TwitchLib.Client;
using TwitchLib.Client.Events;
using TwitchLib.Client.Models;
using TwitchLib.Communication.Clients;
using TwitchLib.Communication.Events;
using static OmniscienceBotCore.Startup;

namespace OmniscienceBotCore.Services
{
    public class TwitchClientWrapper : BackgroundService
    {
        TwitchClient twitchClient;
        TwitchAPI twitchAPI;

        private readonly ILogger logger;
        Thread observer;

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    //await new HttpClient().GetStringAsync("https://omnisciencebot.com/");
                }
                catch (Exception e) { logger.LogError(e.Message); }
                await Task.Delay(TimeSpan.FromMinutes(2));
            }
        }

        TwitchChannels TwitchChannels;
        public readonly IOptions<AppConfig> config;

        public TwitchClientWrapper(TwitchChannels twitchChannels, IOptions<AppConfig> options, TwitchAPI twitchAPI, ILogger logger)
        {
            this.logger = logger;
            config = options;
            this.twitchAPI = twitchAPI;
            TwitchChannels = twitchChannels;
            InitTwitchClient();
            observer = new Thread(ConnectionObserverLoop);
            observer.Start();

            InitChannels();
            Connect();
        }

        bool NotConnectedCheck => !twitchClient.IsConnected || !twitchClient.JoinedChannels.Select(c => c.Channel).OrderBy(c => c).SequenceEqual(TwitchChannels.Channels.Keys.OrderBy(c => c));

        void ConnectionObserverLoop(object o)
        {
            while (true)
            {
                Thread.Sleep(new TimeSpan(0, 2, 0));

                if (NotConnectedCheck)
                {
                    try
                    {
                        if (!twitchClient.IsConnected) Connect();
                    }
                    catch { }
                }

            }
        }

        void InitTwitchClient()
        {
            twitchClient = new TwitchClient(new WebSocketClient());
            twitchClient.Initialize(new ConnectionCredentials("OmniscienceBot", config.Value.TwitchClientOAuth));

            twitchClient.OnConnected += onConnected;
            twitchClient.OnDisconnected += onDisconnected;
            twitchClient.OnMessageReceived += globalChatMessageReceived;
            twitchClient.OnChatCommandReceived += Client_OnChatCommandReceived;
            twitchClient.OnNewSubscriber += Client_OnNewSubscriber;
            twitchClient.OnReSubscriber += Client_OnReSubscriber;
            twitchClient.OnGiftedSubscription += Client_OnGiftedSubscription;
            twitchClient.OnJoinedChannel += Client_OnJoinedChannel;
            twitchClient.OnLeftChannel += Client_OnLeftChannel;
            twitchClient.OnError += Client_OnError;

            twitchClient.OnRaidNotification += TwitchClient_OnRaidNotification;

            twitchClient.AddChatCommandIdentifier('!');
            twitchClient.AddWhisperCommandIdentifier('!');

        }

        void Client_OnError(object sender, OnErrorEventArgs e)
        {
            throw e.Exception;
        }

        public void Connect() => twitchClient.Connect();

        public void JoinAllChannels()
        {
            var t = twitchClient.JoinedChannels.Select(c => c.Channel);
            foreach (var twitchChannel in TwitchChannels.Channels.Values.Where(channel => channel.ChannelSettings.IsStreamer && channel.ChannelSettings.Active)
                                                                        .Select(channel => channel.ChannelSettings.Channel)
                                                                        .Except(t))
                Join(twitchChannel);
        }
        void InitChannels() => TwitchChannels.InitChannels(this);

        public void Join(string channel) => twitchClient.JoinChannel(channel);

        public void Leave(string channel)
        {
            if (twitchClient.JoinedChannels.Count(c => c.Channel == channel) > 0)
                twitchClient.LeaveChannel(channel);
        }

        #region Twitch API calls

        async void SendAnnouncement(string channelID, string colorName, string message)
        {
            AnnouncementColors color = AnnouncementColors.Primary;

            switch (colorName)
            {
                case "blue": color = AnnouncementColors.Blue; break;
                case "green": color = AnnouncementColors.Green; break;
                case "orange": color = AnnouncementColors.Orange; break;
                case "purple": color = AnnouncementColors.Purple; break;
            }

            await twitchAPI.Helix.Chat.SendChatAnnouncementAsync(channelID, config.Value.TwitchBotUserId, message, color);
        }

        public async void SendShoutout(string fromChannelID, string toChannelID)
        {
            await twitchAPI.Helix.Chat.SendShoutoutAsync(
                fromChannelID,
                toChannelID,
                config.Value.TwitchBotUserId);
        }

        public async void SetFollowerOnly(string channelID, bool up = true, int duration = 1440)
        {
            await twitchAPI.Helix.Chat.UpdateChatSettingsAsync(
                channelID,
                config.Value.TwitchBotUserId,
                new TwitchLib.Api.Helix.Models.Chat.ChatSettings.ChatSettings() { FollowerMode = up, FollowerModeDuration = up ? duration : null });
        }

        public async void SetEmoteOnly(string channel, bool up = true)
        {
            await twitchAPI.Helix.Chat.UpdateChatSettingsAsync(
                TwitchChannels[channel].ChannelID,
                config.Value.TwitchBotUserId,
                new TwitchLib.Api.Helix.Models.Chat.ChatSettings.ChatSettings() { EmoteMode = up });
        }

        public async void SetSubOnly(string channel, bool up = true)
        {
            await twitchAPI.Helix.Chat.UpdateChatSettingsAsync(
                TwitchChannels[channel].ChannelID,
                config.Value.TwitchBotUserId,
                new TwitchLib.Api.Helix.Models.Chat.ChatSettings.ChatSettings() { SubscriberMode = up });
        }

        public async void ClearChat(string channel)
        {
            await twitchAPI.Helix.Moderation.DeleteChatMessagesAsync(
                TwitchChannels[channel].ChannelID,
                config.Value.TwitchBotUserId);
        }

        public void SendMessage(string channel, string message)
        {
            var twitchChannel = TwitchChannels[channel];

            if (twitchChannel.ChannelSettings.IsStreamer && twitchChannel.ChannelSettings.Active && !twitchChannel.ChannelSettings.SilenceMode)
                if (message.StartsWith("/announce"))
                    SendAnnouncement(twitchChannel.ChannelID,
                  message.Substring("/announce".Length, message.IndexOf(' ') - "/announce".Length),
                    message.Substring(message.IndexOf(' ') + 1));
                else twitchClient.SendMessage(channel, message);
        }

        public void Timeout(string channel, string username, int seconds) => Ban(channel, username, seconds);
        public void TimeoutById(string channel, string id, int seconds) => BanById(channel, id, seconds);

        public void Ban(string channel, string username, int? seconds = null)
        {
            var user = twitchAPI.Helix.Users.GetUsersAsync(logins: new List<string>() { username }).Result.Users.First();

            if (user != null) BanById(channel, user.Id, seconds);
        }

        public void BanById(string channel, string id, int? seconds = null)
        {
            twitchAPI.Helix.Moderation.BanUserAsync(TwitchChannels[channel].ChannelID, config.Value.TwitchBotUserId,
                new BanUserRequest() { UserId = id, Reason = "", Duration = seconds });
        }

        #endregion

        #region Spotify
        public async void SpotifyPlay(string channel)
        {
            //if (TwitchChannels[channel].Online)
            {
                var status = await TwitchChannels[channel].Spotify.Play();

                if (status.Message != "")
                    SendMessage(channel, status.Message);
            }
        }

        public async void SpotifyPause(string channel)
        {
            //if (TwitchChannels[channel].Online)
            {
                var status = await TwitchChannels[channel].Spotify.Pause();

                if (status.Message != "")
                    SendMessage(channel, status.Message);
            }
        }

        public async void SpotifySkip(string channel)
        {
            if (TwitchChannels[channel].Online)
            {
                var status = await TwitchChannels[channel].Spotify.Skip();

                if (status.Message != "")
                    SendMessage(channel, status.Message);
            }
        }

        public async void SpotifyAdd(string channel, string uri)
        {
            if (TwitchChannels[channel].Online)
            {
                var status = await TwitchChannels[channel].Spotify.Add(uri);

                if (status.Message != "")
                    SendMessage(channel, status.Message);
            }
        }
        #endregion

        TwitchChannelRole GetChannelRole(ChatMessage chatMessage)
        {
            if (chatMessage.IsBroadcaster) return TwitchChannelRole.Broadcaster;
            if (chatMessage.IsModerator) return TwitchChannelRole.Moderator;
            if (TwitchChannels[chatMessage.Channel].Trustworthy.Contains(chatMessage.Username)) return TwitchChannelRole.Trustworthy;
            if (chatMessage.IsSubscriber || chatMessage.Badges.Exists((k) => k.Key == "founder")) return TwitchChannelRole.Subscriber;
            return TwitchChannelRole.User;
        }

        public async Task<TwitchChannelRole> GetChannelRole(string channelId, string userId)
        {
            if (channelId == userId) return TwitchChannelRole.Broadcaster;
            var modRole = (await twitchAPI.Helix.Moderation.GetModeratorsAsync(channelId, new List<string>() { userId })).Data;
            if (modRole != null) return TwitchChannelRole.Moderator;
            var subRole = (await twitchAPI.Helix.Subscriptions.GetUserSubscriptionsAsync(channelId, new List<string>() { userId })).Data;
            if (subRole != null) return TwitchChannelRole.Subscriber;
            return TwitchChannelRole.User;
        }

        public async Task<TwitchChannelRole> GetChannelRoleByUserName(string channelId, string userName)
        {
            var user = (await twitchAPI.Helix.Users.GetUsersAsync(logins: new List<string>() { userName })).Users.FirstOrDefault();

            if (user != null) return await GetChannelRole(channelId, user.Id);
            return TwitchChannelRole.User;
        }

        private void Client_OnNewSubscriber(object sender, OnNewSubscriberArgs e)
        {
            if (TwitchChannels[e.Channel].ChannelSettings.NewSubReactionFormat != null && TwitchChannels[e.Channel].ChannelSettings.NewSubReactionFormat != "")
                SendMessage(e.Channel, TwitchChannels[e.Channel].ChannelSettings.NewSubReactionFormat.Replace("@source", e.Subscriber.DisplayName));
        }

        private void Client_OnReSubscriber(object sender, OnReSubscriberArgs e)
        {
            if (TwitchChannels[e.Channel].ChannelSettings.ReSubReactionFormat != null && TwitchChannels[e.Channel].ChannelSettings.ReSubReactionFormat != "")
                SendMessage(e.Channel, TwitchChannels[e.Channel].ChannelSettings.ReSubReactionFormat.Replace("@source", e.ReSubscriber.DisplayName).Replace("@streak", e.ReSubscriber.MsgParamCumulativeMonths));
        }

        private void Client_OnGiftedSubscription(object sender, OnGiftedSubscriptionArgs e)
        {
            if (TwitchChannels[e.Channel].ChannelSettings.GiftedSubReactionFormat != null && TwitchChannels[e.Channel].ChannelSettings.GiftedSubReactionFormat != "")
                SendMessage(e.Channel, TwitchChannels[e.Channel].ChannelSettings.GiftedSubReactionFormat.Replace("@source", e.GiftedSubscription.MsgParamRecipientDisplayName).Replace("@gifter", e.GiftedSubscription.DisplayName));
        }

        private void Client_OnJoinedChannel(object sender, OnJoinedChannelArgs e)
        {
            TwitchChannels[e.Channel].LoopStart();
        }

        private void Client_OnLeftChannel(object sender, OnLeftChannelArgs e)
        {
            TwitchChannels[e.Channel].LoopAbort();
        }

        async Task<string> GetFollowTime(string userId, string channelId)
        {
            var followData = await twitchAPI.Helix.Users.GetUsersFollowsAsync(fromId: userId, toId: channelId);
            if (followData == null || followData.Follows.Count() == 0) return "peepoWeird";

            DateTime zeroTime = new DateTime(1, 1, 1);
            DateTime olddate = followData.Follows.Last().FollowedAt;
            DateTime curdate = DateTime.UtcNow;
            TimeSpan span = curdate - olddate;

            int years = (zeroTime + span).Year - 1;
            int months = (zeroTime + span).Month - 1;
            int days = (zeroTime + span).Day - 1;

            return (years == 0 ? "" : $" {years} {(years == 1 ? "year" : "years")}") + (months == 0 ? "" : $" {months} {(months == 1 ? "month" : "months")}") + (days == 0 ? "" : $" {days} {(days == 1 ? "day" : "days")}");
        }

        private async void Client_OnChatCommandReceived(object sender, OnChatCommandReceivedArgs e)
        {
            try
            {
                if (e.Command.CommandIdentifier == '!')
                {
                    //if (TwitchChannels[e.Command.ChatMessage.Channel].Commands.Exists(e.Command.CommandText.ToLower()))
                    //{
                    //    var cmd = TwitchChannels[e.Command.ChatMessage.Channel].Commands[e.Command.CommandText.ToLower()] as TextCommand;
                    //    if (cmd.CommandHandler == CommandHandler.Both || cmd.CommandHandler == CommandHandler.Stream) CommandHandle(e.Command, cmd);
                    //}

                    if (e.Command.CommandText.ToLower() == "followtime")
                    {
                        string dt = GetFollowTime(e.Command.ChatMessage.UserId, TwitchChannels[e.Command.ChatMessage.Channel].ChannelID).Result;
                        if (dt != null) SendMessage(e.Command.ChatMessage.Channel, dt == "peepoWeird" ? dt : $"@{e.Command.ChatMessage.DisplayName} follows {e.Command.ChatMessage.Channel} for {dt}.".Replace("  ", " "));
                    }

                    if (e.Command.CommandText.ToLower() == "roll")
                    {
                        Random rnd = new Random();
                        SendMessage(e.Command.ChatMessage.Channel, string.Format("@{0} rolled {1}", e.Command.ChatMessage.DisplayName, rnd.Next(1, 101).ToString()));
                    }

                    //if (TwitchChannels[e.Command.ChatMessage.Channel].StreamUpTime != null)
                    {

                        if (e.Command.CommandText.ToLower() == "song")
                        {
                            var status = await TwitchChannels[e.Command.ChatMessage.Channel].Spotify.Song();

                            if (status.Status != "OK") SendMessage(e.Command.ChatMessage.Channel, status.Message);
                            else SendMessage(e.Command.ChatMessage.Channel, status.Message);
                        }

                        //if (e.Command.CommandText.ToLower() == "songuri" && GetChannelRole(e.Command.ChatMessage) >= ChannelRole.Moderator)
                        //{
                        //    string res;
                        //    string status = TwitchChannels[e.Command.ChatMessage.Channel].Spotify.SongURI(out res);

                        //    if (status != "OK") SendMessage(e.Command.ChatMessage.Channel, status);
                        //    else SendMessage(e.Command.ChatMessage.Channel, res);
                        //}

                        //if (e.Command.CommandText.ToLower() == "songlink" && GetChannelRole(e.Command.ChatMessage) >= ChannelRole.Moderator)
                        //{
                        //    string res;
                        //    string status = TwitchChannels[e.Command.ChatMessage.Channel].Spotify.SongLink(out res);

                        //    if (status != "OK") SendMessage(e.Command.ChatMessage.Channel, status);
                        //    else SendMessage(e.Command.ChatMessage.Channel, res);
                        //}

                        //if (e.Command.CommandText.ToLower() == "forceonline" && (e.Command.ChatMessage.IsBroadcaster || e.Command.ChatMessage.Username.ToLower() == "niliam"))
                        //{
                        //    TwitchChannels[e.Command.ChatMessage.Channel].ForceOnline();
                        //}
                    }

                    #region SongRequests
                    //if (e.Command.CommandText == "sro")
                    //{
                    //    SongRequestHandle(e.Command);
                    //}

                    //if (e.Command.CommandText.ToLower() == "voteskip")
                    //{
                    //    int toSkipCnt = 0;
                    //    Dispatcher.Invoke(new Action(() => toSkip = Convert.ToInt32(VoteSkipTB.Text)));
                    //    if (toSkip == 0)
                    //        return;
                    //    if (voters.Contains(e.Command.ChatMessage.DisplayName))
                    //    {
                    //        toSkip++;
                    //        voters.Add(e.Command.ChatMessage.DisplayName);

                    //        if (toSkip >= toSkipCnt)
                    //        {
                    //            toSkip = 0;
                    //            voters.Clear();
                    //            SongCommand = "skip";
                    //        }
                    //    }
                    //}
                    //if (e.Command.CommandText.ToLower() == "songs" && (e.Command.ChatMessage.IsBroadcaster || e.Command.ChatMessage.IsModerator))
                    //{
                    //    SongCommand = e.Command.ArgumentsAsString;
                    //}
                    #endregion
                }
            }
            catch (Exception ex) { }
        }

        public void DeleteChatMessage(string channel, string msgID)
        {
            twitchAPI.Helix.Moderation.DeleteChatMessagesAsync(TwitchChannels[channel].ChannelID, config.Value.TwitchBotUserId, msgID);
            //client.SendMessage(channel, $"/delete {msgID}");
        }

        void CommandHandle(ChatMessage message, TextCommand command, string commandKey, string userTag = "")
        {
            if (!command.CanExecute(GetChannelRole(message))) return;
            if (command is TextCommand)
                SendMessage(message.Channel, userTag + TwitchChannels[message.Channel].Commands[commandKey].Execute(GetChannelRole(message)).ToString());
        }

        void SongRequestHandle(ChatCommand chatCommand)
        {
            string url = string.Join(" ", chatCommand.ChatMessage.Message.Split(' ').Skip(1));
            string title = "";
            string id = url;
            if (!url.Contains("www.youtube.com")) url = @"https://www.youtube.com/results?search_query=" + url;
            HttpWebRequest myRequest = (HttpWebRequest)HttpWebRequest.Create(url);
            HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
            StreamReader sr = new StreamReader(myResponse.GetResponseStream(), Encoding.UTF8);
            string html = sr.ReadToEnd();
            sr.Close();

            Regex regex = new Regex(@"(https://i.ytimg.com/vi/)(?<url>\S*)(/)");
            Match match = regex.Match(html);

            //url = "https://www.youtube.com/embed/" + match.Groups["url"].Value;
            id = match.Groups["url"].Value;

            regex = new Regex("(\"  title=\")(?<name>.*)(\" rel=\"spf-prefetch\")");
            var matches = regex.Matches(html);
            title = regex.Match(html).Groups["name"].Value;
            // http://www.donationalerts.ru/widget/media?token=27IkQSEcHdR6Gf4x19lo старт видео = появление высоты, отличной от нуля
            TwitchChannels[chatCommand.ChatMessage.Channel].SongRequestQueue.Enqueue(new Song(chatCommand.ChatMessage.Username, id, title));
        }

        public async Task<string> GetUserId(string username)
        {
            string userID;
            var userList = (await twitchAPI.Helix.Users.GetUsersAsync(new List<string>() { username })).Users;

            if (userList == null || userList.Length == 0)
                userID = string.Empty;
            else
                userID = userList[0].Id.Trim();

            return userID;
        }

        private void globalChatMessageReceived(object sender, OnMessageReceivedArgs e)
        {
            if (!e.ChatMessage.IsMe)
            {
                TwitchChannels[e.ChatMessage.Channel].MessagesBefore++;

                foreach (var commandKey in e.ChatMessage.Message.Split(' ').Where(s => s.StartsWith('!')))
                {
                    var key = commandKey[1..].ToLower();

                    if (TwitchChannels[e.ChatMessage.Channel].Commands.Exists(key))
                    {
                        var command = TwitchChannels[e.ChatMessage.Channel].Commands[key] as TextCommand;
                        if (command.CommandHandler == CommandHandler.Both || command.CommandHandler == CommandHandler.Stream)
                            CommandHandle(e.ChatMessage, command, key, e.ChatMessage.ChatReply == null ? "" : $"@{e.ChatMessage.ChatReply.ParentDisplayName} ");
                    }
                }

                TwitchChannels.ProcessEvents(e.ChatMessage, GetChannelRole(e.ChatMessage));
                TwitchChannels[e.ChatMessage.Channel].ProcessEvents(e.ChatMessage, GetChannelRole(e.ChatMessage));
                TwitchChannels[e.ChatMessage.Channel].ProcessCounters(e.ChatMessage, GetChannelRole(e.ChatMessage));
                if (e.ChatMessage.CustomRewardId != null && e.ChatMessage.CustomRewardId != "" && e.ChatMessage.Message.ToLower() == "!getid")
                    SendMessage(e.ChatMessage.Channel, e.ChatMessage.CustomRewardId);
            }
        }

        private void TwitchClient_OnRaidNotification(object sender, OnRaidNotificationArgs e)
        {

            TwitchChannels[e.Channel].CancelFollowerMode(Convert.ToInt32(e.RaidNotification.MsgParamViewerCount));
            TwitchChannels[e.Channel].SendShoutout(e.RaidNotification.UserId, Convert.ToInt32(e.RaidNotification.MsgParamViewerCount));
        }

        public void onConnected(object sender, OnConnectedArgs e)
        {
            if (TwitchChannels.Channels == null || TwitchChannels.Channels.Count == 0) InitChannels();
            JoinAllChannels();
        }

        public void onDisconnected(object sender, OnDisconnectedEventArgs e)
        {
            logger.LogInformation("Twitch Client disconnected.");
            Connect();
        }
    }
}