﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Policy;
using System.Threading;
using System.Threading.Tasks;
using TwitchLib.Api;
using static OmniscienceBotCore.Startup;

namespace OmniscienceBotCore.Services
{
    public class IISAwakenerService : BackgroundService
    {
        private readonly IOptions<AppConfig> options;
        private readonly TwitchAPI twitchAPI;
        private readonly ILogger logger;

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    var validateResponse = await twitchAPI.Auth.ValidateAccessTokenAsync();
                    if (validateResponse == null || validateResponse.Login == null || TimeSpan.FromSeconds(validateResponse.ExpiresIn) < TimeSpan.FromMinutes(3))
                    {
                        var refreshResponse = await twitchAPI.Auth.RefreshAuthTokenAsync(options.Value.TwitchBotRefreshToken, options.Value.TwitchClientAppClientSecret, options.Value.TwitchClientAppId);
                        if (refreshResponse != null)
                        {
                            options.Value.TwitchBotAccessToken = refreshResponse.AccessToken;
                            options.Value.TwitchBotRefreshToken = refreshResponse.RefreshToken;

                            twitchAPI.Settings.AccessToken = refreshResponse.AccessToken;
                        }
                    }
                    //await new HttpClient().GetStringAsync("https://omnisciencebot.com/");
                    //var t = new StreamReader(WebRequest.Create("https://localhost:44342/Home/Poke").GetResponse().GetResponseStream()).ReadLine();
                    //var pong = await new HttpClient().GetStringAsync("https://omnisciencebot.com/Home/Poke", stoppingToken);
                    //logger.LogTrace(pong);
                }
                catch (Exception e) { logger.LogError(e.Message); }
                await Task.Delay(TimeSpan.FromMinutes(3));
            }
        }

        public IISAwakenerService(IOptions<AppConfig> options, TwitchAPI twitchAPI, ILogger logger)
        {
            this.options = options;
            this.twitchAPI = twitchAPI;
            this.logger = logger;
        }
    }
}
