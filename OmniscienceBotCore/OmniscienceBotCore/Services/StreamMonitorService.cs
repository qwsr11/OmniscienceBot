﻿using OmniscienceBotCore.Channel;
using System.Linq;
using TwitchLib.Api;
using TwitchLib.Api.Services;
using TwitchLib.Communication.Interfaces;

namespace OmniscienceBotCore.Services
{
    public class StreamMonitorService
    {
        LiveStreamMonitorService monitor;

        public StreamMonitorService(TwitchAPI api, TwitchChannels channels)
        {
            monitor = new LiveStreamMonitorService(api);

            monitor.SetChannelsById(channels.Channels.Values.Select(c => c.ChannelID).ToList());
            monitor.OnStreamOnline += channels.Monitor_OnStreamOnline;
            monitor.OnStreamOffline += channels.Monitor_OnStreamOffline;

            monitor.Start();
        }
    }
}