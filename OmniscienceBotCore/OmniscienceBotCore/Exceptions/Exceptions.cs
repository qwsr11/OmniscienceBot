﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Exceptions
{
    public class ChannelNotFoundException : ArgumentException
    {
        public string Channel { get; private set; }
        public ChannelNotFoundException(string channel) : base($"Channel {channel} not found.")
        {
            Channel = channel;
        }
    }
}
