using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OmniscienceBotCore.Channel;
using OmniscienceBotCore.Services;
using System;
using System.IO;
using System.Net;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using TwitchLib.Api;
using TwitchLib.PubSub;

namespace OmniscienceBotCore
{
    public class Startup
    {
        public class AppConfig
        {
            public string DebugRedirectUri { get; set; }
            public string ReleaseRedirectUri { get; set; }
            public string ReleaseIPRedirectUri { get; set; }


            public string TwitchClientAppId { get; set; }
            public string TwitchClientAppClientSecret { get; set; }

            public string TwitchBotUserId { get; set; }

            //public string TwitchClientId { get; set; }
            //public string TwitchClientSecret { get; set; }
            public string TwitchBotAccessToken { get; set; }
            public string TwitchBotRefreshToken { get; set; }

            public string TwitchClientOAuth { get; set; }
            public string TwitchClientOAuthRefresh { get; set; }

            public string SpotifyClientId { get; set; }
            public string SpotifyClientSecret { get; set; }

            public string DiscordToken { get; set; }
        }

        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.Configure<AppConfig>(Configuration.GetSection("Config"));

            services.AddLogging(opt =>
            {
                opt.AddConsole(c =>
                {
                    c.UseUtcTimestamp = true;
                    c.TimestampFormat = "[yyyy-MM-dd HH:mm:ss] ";
                });
            });

            services.AddSingleton(sp =>
            {
                var config = sp.GetService<IOptions<AppConfig>>();
                TwitchAPI TwitchAPI = new TwitchAPI();
                TwitchAPI.Settings.ClientId = config.Value.TwitchClientAppId;
                TwitchAPI.Settings.Secret = config.Value.TwitchClientAppClientSecret;
                TwitchAPI.Settings.AccessToken = config.Value.TwitchBotAccessToken;

                return TwitchAPI;
            });

            services.AddSingleton(sp => new IISAwakenerService(sp.GetService<IOptions<AppConfig>>(), sp.GetService<TwitchAPI>(), sp.GetService<ILogger<IISAwakenerService>>()));
            services.AddHostedService(sp => sp.GetService<IISAwakenerService>());

            services.AddSingleton(sp => new DiscordWrapper(sp.GetService<IOptions<AppConfig>>(), sp.GetService<ILogger<DiscordWrapper>>()));
            //services.AddHostedService(sp => sp.GetService<DiscordWrapper>());

            services.AddSingleton(sp => new TwitchChannels(sp.GetService<DiscordWrapper>(), sp.GetService<TwitchAPI>(), sp.GetService<ILogger<TwitchChannels>>()));
            //services.AddHostedService(sp => sp.GetService<TwitchChannels>());

            services.AddSingleton(sp => new TwitchClientWrapper(sp.GetService<TwitchChannels>(), sp.GetService<IOptions<AppConfig>>(), sp.GetService<TwitchAPI>(), sp.GetService<ILogger<TwitchClientWrapper>>()));
            //services.AddHostedService(sp => sp.GetService<TwitchClientWrapper>());

            //services.AddSingleton(sp => new TwitchPubSubWrapper(sp.GetService<TwitchChannels>(), sp.GetService<ILogger<TwitchPubSubWrapper>>(), sp.GetService<ILogger<TwitchPubSub>>()));
            //services.AddHostedService(sp => sp.GetService<TwitchPubSubWrapper>());

            services.AddSingleton(sp => new TwitchPubSubWrapper(sp.GetService<TwitchChannels>(), sp.GetService<ILogger<TwitchPubSubWrapper>>(), sp.GetService<ILogger<TwitchPubSub>>()));

            services.AddSingleton(sp => new StreamMonitorService(sp.GetService<TwitchAPI>(), sp.GetService<TwitchChannels>()));

            services.AddControllersWithViews().AddRazorRuntimeCompilation();
            services.AddMvc().AddSessionStateTempDataProvider();

            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true; // consent required
            });
            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromHours(29);
                // You might want to only set the application cookies over a secure connection:
                options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
                options.Cookie.SameSite = SameSiteMode.Strict;
                options.Cookie.HttpOnly = true;
                // Make the session cookie essential
                options.Cookie.IsEssential = true;
            });
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(options =>
            {
                options.LoginPath = new Microsoft.AspNetCore.Http.PathString("/Home/Login");
                options.AccessDeniedPath = new Microsoft.AspNetCore.Http.PathString("/Home/AccessDenied");
                ///https://stackoverflow.com/questions/34979680/asp-net-core-mvc-setting-expiration-of-identity-cookie
                options.ExpireTimeSpan = TimeSpan.FromHours(1);
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("Events", policy =>
                    policy.RequireAssertion(context =>
                    context.User.HasClaim(claim => claim.Type == ClaimsIdentity.DefaultRoleClaimType && "Admin,Streamer".Contains(claim.Value)) ||
                    (context.User.HasClaim(claim => claim.Type == ClaimsIdentity.DefaultRoleClaimType && claim.Value == "Moderator") &&
                    context.User.HasClaim(claim => claim.Type == "Scopes" && claim.Value.Contains("Events")))
                    ));

                options.AddPolicy("Commands", policy =>
                    policy.RequireAssertion(context =>
                    context.User.HasClaim(claim => claim.Type == ClaimsIdentity.DefaultRoleClaimType && "Admin,Streamer".Contains(claim.Value)) ||
                    (context.User.HasClaim(claim => claim.Type == ClaimsIdentity.DefaultRoleClaimType && claim.Value == "Moderator") &&
                    context.User.HasClaim(claim => claim.Type == "Scopes" && claim.Value.Contains("Commands")))
                    ));

                options.AddPolicy("Pastes", policy =>
                    policy.RequireAssertion(context =>
                    context.User.HasClaim(claim => claim.Type == ClaimsIdentity.DefaultRoleClaimType && "Admin,Streamer".Contains(claim.Value)) ||
                    (context.User.HasClaim(claim => claim.Type == ClaimsIdentity.DefaultRoleClaimType && claim.Value == "Moderator") &&
                    context.User.HasClaim(claim => claim.Type == "Scopes" && claim.Value.Contains("Pastes")))
                    ));

                options.AddPolicy("Counters", policy =>
                    policy.RequireAssertion(context =>
                    context.User.HasClaim(claim => claim.Type == ClaimsIdentity.DefaultRoleClaimType && "Admin,Streamer".Contains(claim.Value)) ||
                    (context.User.HasClaim(claim => claim.Type == ClaimsIdentity.DefaultRoleClaimType && claim.Value == "Moderator") &&
                    context.User.HasClaim(claim => claim.Type == "Scopes" && claim.Value.Contains("Counters")))
                    ));

                options.AddPolicy("StreamAnnouncements", policy =>
                    policy.RequireAssertion(context =>
                    context.User.HasClaim(claim => claim.Type == ClaimsIdentity.DefaultRoleClaimType && "Admin,Streamer".Contains(claim.Value)) ||
                    (context.User.HasClaim(claim => claim.Type == ClaimsIdentity.DefaultRoleClaimType && claim.Value == "Moderator") &&
                    context.User.HasClaim(claim => claim.Type == "Scopes" && claim.Value.Contains("Counters")))
                    ));
            });


            services.AddDataProtection()
.SetApplicationName("OmniscienceBot")
.PersistKeysToFileSystem(new DirectoryInfo(@".\GeneratedEncryptionKeys"));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSession();
            app.UseAuthentication();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            //app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();
            app.UseForwardedHeaders(new ForwardedHeadersOptions()
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedProto
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}