﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OmniscienceBotCore.Channel;
using OmniscienceBotCore.Exceptions;
using OmniscienceBotCore.Models;
using OmniscienceBotCore.Services;

namespace OmniscienceBotCore.Controllers
{
    [Authorize(Roles = "Admin, Streamer")]
    public class ModeratorsController : Controller
    {
        TwitchChannels TwitchChannels;
        TwitchClientWrapper twitchClient;

        public ModeratorsController(TwitchChannels twitchChannels, TwitchClientWrapper twitchClient)
        {
            TwitchChannels = twitchChannels;
            this.twitchClient = twitchClient;
        }

        // GET: Moderators
        public async Task<IActionResult> Index()
        {
            string channel = User.Claims.First(c => c.Type == ClaimsIdentity.DefaultNameClaimType).Value;
            if (channel != null)
                try { return View(TwitchChannels[channel].Moderators.Collection); }
                catch (ChannelNotFoundException e) { return RedirectToAction("Error", "Home", new { @message = (object)e.Message }); }
            else return RedirectToAction("Index", "Home");
        }

        // GET: Moderators/Create
        public IActionResult Create()
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null) return View(new ModeratorsViewModel(channel));
            else return RedirectToAction("Index", "Home");
        }


        //https://stackoverflow.com/questions/59631459/cant-save-multiple-selections-in-select-list-asp-net-core-web-app-razor-pages

        // POST: Moderators/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ModeratorsViewModel moderators)
        {
            if (ModelState.IsValid && moderators is not null)
            {
                if (moderators.ModName is not null && moderators.ModName != "" && moderators.SelectedScopes.Any())
                {
                    string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
                    if (channel != null)
                    {
                        TwitchChannels[channel].Moderators.Add(new Moderator()
                        {
                            Channel = channel,
                            ModName = moderators.ModName,
                            Scopes = string.Join(";", moderators.SelectedScopes)
                        });
                        return RedirectToAction("Index");
                    }
                    else return RedirectToAction("Index", "Home");
                }
                else return RedirectToAction("Index");
            }
            return View();
        }

        // GET: Moderators/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null)
            {
                var mod = TwitchChannels[channel].Moderators[id];
                if (mod == null)
                    return NotFound();

                return View(new ModeratorsViewModel(channel, mod.ModName, mod.Scopes.Split(';').ToList()));
            }
            return RedirectToAction("Index", "Home");
        }

        // POST: Moderators/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, ModeratorsViewModel moderator)
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null)
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        if (moderator.ModName is not null && moderator.ModName != "" && moderator.SelectedScopes.Any())
                        {
                            TwitchChannels[channel].Moderators.Change(new Moderator()
                            {
                                Id = id,
                                Channel = channel,
                                ModName = moderator.ModName,
                                Scopes = string.Join(";", moderator.SelectedScopes)
                            });
                        }
                    }
                    catch (Exception ex)
                    {
                        return RedirectToAction("Index");
                    }
                }
            }
            return RedirectToAction("Index");
        }

        // GET: Moderators/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            string channel = User.Claims.First(c => c.Type == ClaimsIdentity.DefaultNameClaimType).Value;
            if (channel != null)
            {
                var mod = TwitchChannels[channel].Moderators[id];
                if (mod == null)
                    return NotFound();

                return View(new ModeratorsViewModel(channel, mod.ModName, mod.Scopes.Split(';').ToList()));
            }
            else return RedirectToAction("Index", "Home");
        }

        // POST: Moderators/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null) TwitchChannels[channel].Moderators.Delete(id);
            return RedirectToAction("Index");
        }
    }
}