﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using OmniscienceBotCore.Channel;
using OmniscienceBotCore.Exceptions;
using OmniscienceBotCore.Models;
using OmniscienceBotCore.Models.Events;
using OmniscienceBotCore.Services;

namespace OmniscienceBotCore.Controllers
{
    [Authorize(Policy = "Events")]
    public class GlobalEventsController : Controller
    {
        TwitchChannels TwitchChannels;
        TwitchClientWrapper twitchClient;
        public GlobalEventsController(TwitchChannels twitchChannels, TwitchClientWrapper twitchClient)
        {
            TwitchChannels = twitchChannels;
            this.twitchClient = twitchClient;
        }
        // GET: Events
        public ActionResult Index()
        {
            //HttpContext.Session.SetString("key", "");
            string channel = User.Claims.First(c => c.Type == ClaimsIdentity.DefaultNameClaimType).Value;
            if (channel != null)
                try
                {
                    int key = TwitchChannels[channel].Events.CurrentKey;
                    TwitchChannels[channel].Events.CurrentKey = -1;
                    List<EventsViewModel> list = new List<EventsViewModel>();
                    foreach (Event e in TwitchChannels.GlobalEvents.Collection)
                        list.Add(new EventsViewModel(e, channel));

                    return View(list.OrderByDescending(e => e.Subscribed));
                }
                catch (ChannelNotFoundException e) { return RedirectToAction("Error", "Home", new { @message = (object)e.Message }); }
            else return RedirectToAction("Index", "Home");
        }

        public string Fetch()
        {
            string channel = User.Claims.First(c => c.Type == ClaimsIdentity.DefaultNameClaimType).Value;
            if (channel != null)
            {
                int key = TwitchChannels[channel].Events.CurrentKey;
                if (key == -1) return new EventsConstructorWrapper().ToJson();
                else return new EventsConstructorWrapper(Data.FromJson(TwitchChannels.GlobalEvents[key].Json), "", "success").ToJson();
            }
            else return new EventsConstructorWrapper().ToJson();
        }
        [HttpPost]
        [Helpers.RequestSizeLimit(valueCountLimit: 1073741824)]
        public string Fetch(object e)
        {
            Data data = Data.FromJson(Request.Form.Keys.First());
            string channel = User.Claims.First(c => c.Type == ClaimsIdentity.DefaultNameClaimType).Value;

            string msg = Validate(data);
            EventsConstructorWrapper eConstructor = new EventsConstructorWrapper(data, msg, "success");
            if (msg == "OK")
            {
                int key = TwitchChannels[channel].Events.CurrentKey;
                if (key > 0) data.Id = key;
                if (TwitchChannels.GlobalEvents.Exists(key)) TwitchChannels.GlobalEvents.Change(new Event("", twitchClient, data.ToJson(), data.Options.Others.Enabled) { Id = data.Id });
                else TwitchChannels.GlobalEvents.Add(new Event("", twitchClient, data.ToJson(), data.Options.Others.Enabled), TwitchChannels.Channels.Keys.ToList(), channel);
            }
            return eConstructor.ToJson();
        }

        public ActionResult Constructor()
        {
            string channel = User.Claims.First(c => c.Type == ClaimsIdentity.DefaultNameClaimType).Value;
            if (channel != null) return View();
            else return RedirectToAction("Index", "Home");
        }

        public ActionResult Import(int id)
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null) return View(TwitchChannels.GlobalEvents[id]);
            else return RedirectToAction("Index", "Home");
        }
        public ActionResult Export()
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null) return View();
            else return RedirectToAction("Index", "Home");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Export(EventsExportViewModel jsonViewModel)
        {
            if (jsonViewModel != null && jsonViewModel.Json != "")
            {
                string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
                if (channel != null)
                {
                    Data data = Data.FromJson(jsonViewModel.Json);
                    string msg = Validate(data);
                    if (msg == "OK") TwitchChannels.GlobalEvents.Add(new Event("", twitchClient, data.ToJson(), data.Options.Others.Enabled), TwitchChannels.Channels.Keys.ToList(), channel);
                }
                else return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Index");
        }

        public ActionResult Create()
        {
            string channel = User.Claims.First(c => c.Type == ClaimsIdentity.DefaultNameClaimType).Value;
            if (channel != null)
            {
                TwitchChannels[channel].Events.CurrentKey = -1;

                return RedirectToAction("Constructor");
            }
            else return RedirectToAction("Index", "Home");
        }

        // GET: Events/Edit/5
        public ActionResult Edit(int id)
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null)
            {
                TwitchChannels[channel].Events.CurrentKey = id;
                return RedirectToAction("Constructor");
            }
            else return RedirectToAction("Index", "Home");
        }

        // GET: Events/Delete/5
        public ActionResult Delete(int id)
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null) return View(TwitchChannels.GlobalEvents[id]);
            else return RedirectToAction("Index", "Home");
        }

        // POST: Events/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, object p = null)
        {
            if (id != 0)
            {
                string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
                if (channel != null)
                {
                    string editor = User.Claims.First(c => c.Type == ClaimsIdentity.DefaultRoleClaimType).Value == "Moderator" ? User.Claims.First(c => c.Type == "Moderator").Value : channel;
                    TwitchChannels.GlobalEvents.Delete(id, editor);
                }
                else return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Index");
        }

        public ActionResult Sub(int id)
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null)
            {
                TwitchChannels.GlobalEvents.Sub(channel, id);
                return RedirectToAction("Index");
            }
            else return RedirectToAction("Index", "Home");
        }

        public ActionResult Unsub(int id)
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null)
            {
                TwitchChannels.GlobalEvents.Unsub(channel, id);
                return RedirectToAction("Index");
            }
            else return RedirectToAction("Index", "Home");
        }

        #region Validation
        static string ProbabilityDelimeter = "##";
        public string Validate(Data data)
        {
            if (data.Options.Methods == null) return "No methods provided";
            foreach (Method method in data.Options.Methods.Where(m => m.Model != null))
            {
                if (method.Model.Value == "") return "Empty field provided";
                if (!ValidateProbabilty(method.Model.Value)) return "Invalid probablity format";
                switch (method.Name)
                {
                    case "timeoutUser":
                        if (!ValidateTimeout(method.Model.Value)) return "User timeout method has invalid parameters";
                        else break;
                }
            }

            return "OK";
        }
        bool ValidateProbabilty(string value)
        {
            int parameters = value.Split(ProbabilityDelimeter).Length;
            if (parameters == 1) return true;
            if (parameters == 2)
            {
                int p;
                string s = value.Split(ProbabilityDelimeter)[1];
                if (s.Last() == '!') s = s.Remove(s.Length - 1);
                if (int.TryParse(s, out p))
                {
                    if (!(p >= 0 && p <= 100))
                        return false;
                }
                else return false;
            }

            return true;
        }
        bool ValidateTimeout(string value)
        {
            int parameters = value.Split('#').Length;
            if (!(parameters >= 2 && int.TryParse(value.Split('#')[1], out int t))) return false;

            return true;
        }
        #endregion
    }
}