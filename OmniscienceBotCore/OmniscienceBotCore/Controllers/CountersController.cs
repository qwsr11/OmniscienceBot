﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OmniscienceBotCore.Channel;
using OmniscienceBotCore.Exceptions;
using OmniscienceBotCore.Models;

namespace OmniscienceBotCore.Controllers
{
    [Authorize(Policy = "Counters")]
    public class CountersController : Controller
    {
        TwitchChannels TwitchChannels;
        public CountersController(TwitchChannels twitchChannels)
        {
            TwitchChannels = twitchChannels;
        }
        // GET: Counters
        public ActionResult Index()
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null)
                try
                {
                    return View(TwitchChannels[channel].Counters.Collection);
                }
                catch (ChannelNotFoundException e) { return RedirectToAction("Error", "Home", new { @message = (object)e.Message }); }
            else return RedirectToAction("Index", "Home");
        }

        // GET: Counters/Create
        public ActionResult Create()
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null) return View();
            else return RedirectToAction("Index", "Home");
        }

        // POST: Counters/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Counter counter)
        {
            try
            {
                string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
                if (channel != null)
                {
                    string editor = User.Claims.First(c => c.Type == ClaimsIdentity.DefaultRoleClaimType).Value == "Moderator" ? User.Claims.First(c => c.Type == "Moderator").Value : channel;
                    TwitchChannels[channel].Counters.Add(new Counter(channel, counter.Id, counter.HandleMode, counter.TextToFind, counter.ResponseMode, counter.ResponseParams, counter.AnswerPattern), editor);
                }
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Counters/Edit/5
        public ActionResult Edit(int id)
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null) return View(TwitchChannels[channel].Counters[id]);
            else return RedirectToAction("Index", "Home");
        }

        // POST: Counters/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Counter counter)
        {
            if (counter != null && counter.Id != 0)
            {
                string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
                if (channel != null)
                {
                    string editor = User.Claims.First(c => c.Type == ClaimsIdentity.DefaultRoleClaimType).Value == "Moderator" ? User.Claims.First(c => c.Type == "Moderator").Value : channel;
                    TwitchChannels[channel].Counters.Change(counter, editor);
                }
                else return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Index");
        }

        // GET: Counters/Delete/5
        public ActionResult Delete(int id)
        {
            string channel = User.Claims.First(c => c.Type == ClaimsIdentity.DefaultNameClaimType).Value;
            if (channel != null) return View(TwitchChannels[channel].Counters[id]);
            else return RedirectToAction("Index", "Home");
        }

        // POST: Counters/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Counter counter)
        {
            if (counter != null && counter.Id != 0)
            {
                string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
                if (channel != null)
                {
                    string editor = User.Claims.First(c => c.Type == ClaimsIdentity.DefaultRoleClaimType).Value == "Moderator" ? User.Claims.First(c => c.Type == "Moderator").Value : channel;
                    TwitchChannels[channel].Counters.Delete(counter.Id, editor);
                }
                else return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Index");
        }
    }
}