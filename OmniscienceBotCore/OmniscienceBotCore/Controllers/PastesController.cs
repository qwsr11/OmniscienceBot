﻿using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OmniscienceBotCore.Channel;
using OmniscienceBotCore.Exceptions;
using OmniscienceBotCore.Models;

namespace OmniscienceBotCore.Controllers
{
    [Authorize(Policy = "Pastes")]
    public class PastesController : Controller
    {
        TwitchChannels TwitchChannels;
        public PastesController(TwitchChannels twitchChannels)
        {
            TwitchChannels = twitchChannels;
        }
        // GET: Pastes
        public ActionResult Index()
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null)
                try { return View(TwitchChannels[channel].Pastes.Collection); }
                catch (ChannelNotFoundException e) { return RedirectToAction("Error", "Home", new { @message = (object)e.Message }); }
            else return RedirectToAction("Index", "Home");
        }

        // GET: Pastes/Create
        public ActionResult Create()
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null) return View();
            else return RedirectToAction("Index", "Home");
        }

        // POST: Pastes/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Paste paste)
        {
            try
            {
                string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
                if (channel != null)
                {
                    string editor = User.Claims.First(c => c.Type == ClaimsIdentity.DefaultRoleClaimType).Value == "Moderator" ? User.Claims.First(c => c.Type == "Moderator").Value : channel;
                    TwitchChannels[channel].Pastes.Add(new Paste(channel, paste.Key, paste.Text), editor);
                }
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Pastes/Edit/5
        public ActionResult Edit(int id)
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null) return View(TwitchChannels[channel].Pastes[id]);
            else return RedirectToAction("Index", "Home");
        }

        // POST: Pastes/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Paste paste)
        {
            if (paste != null && paste.Id != 0)
            {
                string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
                if (channel != null)
                {
                    string editor = User.Claims.First(c => c.Type == ClaimsIdentity.DefaultRoleClaimType).Value == "Moderator" ? User.Claims.First(c => c.Type == "Moderator").Value : channel;
                    TwitchChannels[channel].Pastes.Change(paste, editor);
                }
                else return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Index");
        }

        // GET: Pastes/Delete/5
        public ActionResult Delete(int id)
        {
            string channel = User.Claims.First(c => c.Type == ClaimsIdentity.DefaultNameClaimType).Value;
            if (channel != null) return View(TwitchChannels[channel].Pastes[id]);
            else return RedirectToAction("Index", "Home");
        }

        // POST: Pastes/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Paste item)
        {
            if (item != null && item.Id != 0)
            {
                string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
                if (channel != null)
                {
                    string editor = User.Claims.First(c => c.Type == ClaimsIdentity.DefaultRoleClaimType).Value == "Moderator" ? User.Claims.First(c => c.Type == "Moderator").Value : channel;
                    TwitchChannels[channel].Pastes.Delete(item.Id, editor);
                }
                else return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Index");
        }
    }
}