﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OmniscienceBotCore.Channel;
using OmniscienceBotCore.Models;
using OmniscienceBotCore.Services;
using TwitchLib.Api;
using static OmniscienceBotCore.Startup;

namespace OmniscienceBotCore.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> logger;
        TwitchAPI twitchAPI;
        private readonly StreamMonitorService streamMonitor;
        private readonly TwitchPubSubWrapper twitchPubSubWrapper;
        TwitchChannels twitchChannels;
        readonly IOptions<AppConfig> config;

        public HomeController(ILogger<HomeController> logger, IOptions<AppConfig> config, TwitchClientWrapper twitchClientWrapper, TwitchChannels twitchChannels, TwitchAPI twitchAPI, StreamMonitorService streamMonitor, TwitchPubSubWrapper twitchPubSubWrapper)
        {
            this.logger = logger;
            this.twitchChannels = twitchChannels;
            this.config = config;
            this.twitchAPI = twitchAPI;
            this.streamMonitor = streamMonitor;
            this.twitchPubSubWrapper = twitchPubSubWrapper;
        }

        [DataContract]
        class TwitchAuthResponse
        {
            [DataMember]
            public string access_token { get; set; }
            [DataMember]
            public string refresh_token { get; set; }
            [DataMember]
            public int expires_in { get; set; }
            [DataMember]
            public List<string> scope { get; set; }
            [DataMember]
            public string token_type { get; set; }
            [IgnoreDataMember]
            public byte[] Json { get; set; }
        }

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Poke()
        {
            return Ok("Pong");
        }
        //[Authorize(Roles = "Unauthorized")]
        public async Task<IActionResult> Login(string code = "", string scope = "")
        {
            string redirect_uri = $"{config.Value.ReleaseRedirectUri}/Home/Login";

#if DEBUG
            redirect_uri = $"{config.Value.DebugRedirectUri}/Home/Login";
#endif
            if (code != "")
            {
                HttpClient httpClient = new HttpClient();

                string grant_type = "authorization_code";

                var values = new Dictionary<string, string>
            {
                { "client_id", config.Value.TwitchClientAppId},
                { "client_secret", config.Value.TwitchClientAppClientSecret},
                { "code", code },
                { "grant_type", grant_type },
                { "redirect_uri", redirect_uri }
            };

                var content = new FormUrlEncodedContent(values);

                var response = await httpClient.PostAsync("https://id.twitch.tv/oauth2/token", content);

                DataContractJsonSerializer dataContractJsonSerializer = new DataContractJsonSerializer(typeof(TwitchAuthResponse));
                var str = await response.Content.ReadAsStringAsync();

                TwitchAuthResponse authResponse = (TwitchAuthResponse)dataContractJsonSerializer.ReadObject(new MemoryStream(Encoding.UTF8.GetBytes(str)));
                authResponse.Json = Encoding.UTF8.GetBytes(str);

                var user = (await twitchAPI.Helix.Users.GetUsersAsync(accessToken: authResponse.access_token)).Users.First();
                string channel = user.Login;

                string role = Roles.GetRole(channel, twitchChannels.Channels);
                var claims = new List<Claim> {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, channel),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, role)
                };
                ClaimsIdentity id = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
            }
            else return base.Redirect($"https://id.twitch.tv/oauth2/authorize?response_type=code&client_id={config.Value.TwitchClientAppId}&redirect_uri={redirect_uri}&scope=user:read:email");
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Index");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult AccessDenied()
        {
            return View();
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error(string message = null)
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier, Channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value, Message = message });
        }
    }
}
