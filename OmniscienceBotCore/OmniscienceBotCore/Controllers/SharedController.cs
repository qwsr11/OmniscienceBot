﻿using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using OmniscienceBotCore.Channel;

namespace OmniscienceBotCore.Controllers
{
    public class SharedController : Controller
    {
        TwitchChannels TwitchChannels;
        public SharedController(TwitchChannels twitchChannels)
        {
            TwitchChannels = twitchChannels;
        }

        public PartialViewResult Login()
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel == null) channel = "";
            return PartialView(channel);
        }
    }

    public class _SongRequestController : Controller
    {
        TwitchChannels TwitchChannels;
        public _SongRequestController(TwitchChannels twitchChannels)
        {
            TwitchChannels = twitchChannels;
        }

        public string CheckForSong()
        {
            //https://www.mikesdotnetting.com/article/325/partials-and-ajax-in-razor-pages
            //http://www.binaryintellect.net/articles/65cfe9cd-b98f-48b7-825d-31ae8008a91a.aspx
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null) return TwitchChannels[channel].SongRequestCheck?.ID;
            else return "";
        }
    }

    public class _AjaxSoundCommandController : Controller
    {
        TwitchChannels TwitchChannels;
        public _AjaxSoundCommandController(TwitchChannels twitchChannels)
        {
            TwitchChannels = twitchChannels;
        }

        public string IsThereAnything()
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null && TwitchChannels.Channels.ContainsKey(channel)) return TwitchChannels[channel].QueueIsEmpty ? "" : "+";
            else return "";
        }

        public PartialViewResult CallAjax()
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null)
            {
                string path = TwitchChannels[channel].CheckForQueue;

                if (path != "") return new PartialViewResult() { ViewName = "_AjaxSoundCommand", ViewData = new Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<string>(ViewData, path) };
                else return null;
            }
            else return null;
        }
    }
}