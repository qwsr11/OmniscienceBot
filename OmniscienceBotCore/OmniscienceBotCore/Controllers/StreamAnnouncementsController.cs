﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OmniscienceBotCore.Channel;
using OmniscienceBotCore.Exceptions;
using OmniscienceBotCore.Models;

namespace OmniscienceBotCore.Controllers
{
    [Authorize(Policy = "StreamAnnouncements")]
    public class StreamAnnouncementsController : Controller
    {
        TwitchChannels TwitchChannels;
        public StreamAnnouncementsController(TwitchChannels twitchChannels)
        {
            TwitchChannels = twitchChannels;
        }
        // GET: Counters
        public ActionResult Index()
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null)
                try
                {
                    return View(TwitchChannels[channel].Announcements.Collection);
                }
                catch (ChannelNotFoundException e) { return RedirectToAction("Error", "Home", new { @message = (object)e.Message }); }
            else return RedirectToAction("Index", "Home");
        }

        // GET: Counters/Create
        public ActionResult Create()
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null) return View();
            else return RedirectToAction("Index", "Home");
        }

        // POST: Counters/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(StreamAnnouncement streamAnnouncement)
        {
            try
            {
                string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
                if (channel != null)
                {
                    string editor = User.Claims.First(c => c.Type == ClaimsIdentity.DefaultRoleClaimType).Value == "Moderator" ? User.Claims.First(c => c.Type == "Moderator").Value : channel;
                    TwitchChannels[channel].Announcements.Add(new StreamAnnouncement(channel, streamAnnouncement.StreamToAnnounce, streamAnnouncement.DiscordChannel, streamAnnouncement.Text, streamAnnouncement.GracePeriod), editor);

                    return RedirectToAction(nameof(Index));
                }
                else return RedirectToAction("Index", "Home");
            }
            catch
            {
                return View();
            }
        }

        // GET: Counters/Edit/5
        public ActionResult Edit(int id)
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null) return View(TwitchChannels[channel].Announcements[id]);
            else return RedirectToAction("Index", "Home");
        }

        // POST: Counters/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(StreamAnnouncement streamAnnouncement)
        {
            if (streamAnnouncement != null && streamAnnouncement.Id != 0)
            {
                string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
                if (channel != null)
                {
                    string editor = User.Claims.First(c => c.Type == ClaimsIdentity.DefaultRoleClaimType).Value == "Moderator" ? User.Claims.First(c => c.Type == "Moderator").Value : channel;
                    TwitchChannels[channel].Announcements.Change(streamAnnouncement, editor);
                }
                else return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            string channel = User.Claims.First(c => c.Type == ClaimsIdentity.DefaultNameClaimType).Value;
            if (channel != null) return View(TwitchChannels[channel].Announcements[id]);
            else return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(StreamAnnouncement streamAnnouncement)
        {
            if (streamAnnouncement != null && streamAnnouncement.Id != 0)
            {
                string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
                if (channel != null)
                {
                    string editor = User.Claims.First(c => c.Type == ClaimsIdentity.DefaultRoleClaimType).Value == "Moderator" ? User.Claims.First(c => c.Type == "Moderator").Value : channel;
                    TwitchChannels[channel].Announcements.Delete(streamAnnouncement.Id, editor);
                }
                else return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Index");
        }
    }
}
