﻿using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OmniscienceBotCore.Channel;
using OmniscienceBotCore.Exceptions;
using OmniscienceBotCore.Models;

namespace OmniscienceBotCore.Controllers
{
    [Authorize(Roles = "Admin, Streamer")]
    public class ChannelSettingsController : Controller
    {
        TwitchChannels TwitchChannels;
        public ChannelSettingsController(TwitchChannels twitchChannels)
        {
            TwitchChannels = twitchChannels;
        }
        public IActionResult Index()
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null)
                try { return View(TwitchChannels[channel].ChannelSettings); }
                catch (ChannelNotFoundException e) { return RedirectToAction("Error", "Home", new { @message = (object)e.Message }); }
            else return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(SettingsMain channelSettings)
        {
            if (channelSettings != null && channelSettings.Channel != "")
            {
                string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
                TwitchChannels[channel].ChannelSettings = channelSettings;
            }
            return RedirectToAction("Index", "Home");
        }
    }
}