﻿using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OmniscienceBotCore.Channel;
using OmniscienceBotCore.Exceptions;
using OmniscienceBotCore.Models;
using OmniscienceBotCore.Models.Commands;

namespace OmniscienceBotCore.Controllers
{
    [Authorize(Policy = "Commands")]
    public class CommandsController : Controller
    {
        TwitchChannels TwitchChannels;
        public CommandsController(TwitchChannels twitchChannels)
        {
            TwitchChannels = twitchChannels;
        }
        public IActionResult Index()
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null)
                try { return View(TwitchChannels[channel].Commands.Collection.Cast<TextCommand>()); }
                catch (ChannelNotFoundException e) { return RedirectToAction("Error", "Home", new { @message = (object)e.Message }); }
            else return RedirectToAction("Index", "Home");
        }

        public IActionResult CreateTextCommand()
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null) return View(new TextCommand() { Channel = channel });
            else return RedirectToAction("Index", "Home");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateTextCommand(TextCommand _cmd)
        {
            if (_cmd != null && _cmd is TextCommand && _cmd.Key != null)
            {
                TextCommand cmd = _cmd as TextCommand;
                string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
                if (channel != null)
                {
                    string editor = User.Claims.First(c => c.Type == ClaimsIdentity.DefaultRoleClaimType).Value == "Moderator" ? User.Claims.First(c => c.Type == "Moderator").Value : channel;
                    cmd.Channel = channel;
                    TwitchChannels[channel].Commands.Add(cmd, editor);
                    return RedirectToAction("Index");
                }
                else return RedirectToAction("Index", "Home");
            }
            return View();
        }
        [HttpGet]
        public IActionResult EditTextCommand(int id)
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null) return View(TwitchChannels[channel].Commands[id]);
            else return RedirectToAction("Index", "Home");
        }
        [HttpPost]
        public IActionResult EditTextCommand(TextCommand command)
        {
            if (command != null && command.Key != "")
            {
                string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
                if (channel != null)
                {
                    string editor = User.Claims.First(c => c.Type == ClaimsIdentity.DefaultRoleClaimType).Value == "Moderator" ? User.Claims.First(c => c.Type == "Moderator").Value : channel;
                    TwitchChannels[channel].Commands.Change(command, editor);
                }
                else return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Index");
        }
        public IActionResult DeleteTextCommand(int id)
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null) return View(TwitchChannels[channel].Commands[id]);
            else return RedirectToAction("Index", "Home");
        }

        public IActionResult DeleteTextCmd(int id)
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null)
            {
                string editor = User.Claims.First(c => c.Type == ClaimsIdentity.DefaultRoleClaimType).Value == "Moderator" ? User.Claims.First(c => c.Type == "Moderator").Value : channel;
                TwitchChannels[channel].Commands.Delete(id, editor);
                return RedirectToAction("Index");
            }
            else return RedirectToAction("Index", "Home");
        }
    }
}