﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OmniscienceBotCore.Channel;
using OmniscienceBotCore.Services;
using SpotifyAPI.Web;
using static OmniscienceBotCore.Startup;

namespace OmniscienceBotCore.Controllers
{
    [Authorize(Roles = "Admin, Streamer")]
    public class SpotifyController : Controller
    {

        private readonly ILogger<HomeController> _logger;
        TwitchClientWrapper TwitchClientWrapper;
        readonly IOptions<AppConfig> config;
        TwitchChannels TwitchChannels;

        public SpotifyController(TwitchChannels twitchChannels, ILogger<HomeController> logger, TwitchClientWrapper twitchClientWrapper, IOptions<AppConfig> config)
        {
            TwitchChannels = twitchChannels;
            _logger = logger;
            TwitchClientWrapper = twitchClientWrapper;
            this.config = config;
        }
        public IActionResult Index()
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null) return View();
            else return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> Login(string code = "")
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel == null) RedirectToAction("Index");

            string clientID = "0771b503300e49d398bec27b62c4b524";
            string clientSecret = "fbe8ace792754b138cfc8e934fe44b30";

            string redirect_uri = $"{config.Value.ReleaseRedirectUri}/Spotify/Login";

#if DEBUG
            redirect_uri = $"{config.Value.DebugRedirectUri}/Spotify/Login";
#endif

            if (code != "")
            {
                var response = await new OAuthClient().RequestToken(new AuthorizationCodeTokenRequest(clientID, clientSecret, code, new Uri(redirect_uri)));

                TwitchChannels[channel].SpotifyInit(response.RefreshToken, response.AccessToken);
            }
            else
            {

                var loginRequest = new LoginRequest(
                    new Uri(redirect_uri),
                    clientID,
                    LoginRequest.ResponseType.Code)
                {
                    Scope = new[]
                    {
                        Scopes.UserReadCurrentlyPlaying,
                        Scopes.UserReadPlaybackState,
                        Scopes.UserModifyPlaybackState
                    }
                };

                return Redirect(loginRequest.ToUri().ToString());
            }
            return RedirectToAction("Index");
        }
    }
}
