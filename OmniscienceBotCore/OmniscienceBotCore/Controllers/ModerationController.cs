﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using OmniscienceBotCore.Channel;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using OmniscienceBotCore.Models;
using Microsoft.AspNetCore.Authorization;

namespace OmniscienceBotCore.Controllers
{
    [Authorize(Roles = "Admin, Streamer, User")]
    public class ModerationController : Controller
    {
        TwitchChannels TwitchChannels;
        public ModerationController(TwitchChannels twitchChannels)
        {
            TwitchChannels = twitchChannels;
        }

        public IActionResult Index()
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null) return View(TwitchChannels.Channels.Values.Where(streamer => streamer.Moderators[channel] != null).Select(streamer => streamer.ChannelSettings.Channel));
            else return RedirectToAction("Index", "Home");
        }

        public async Task<ActionResult> Switch(string streamer)
        {
            if (streamer != null && streamer != "")
            {
                string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
                if (streamer != channel)
                {
                    await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

                    string role = "Moderator";

                    Moderator moderator = TwitchChannels.Channels[streamer].Moderators[channel];

                    var claims = new List<Claim>
                    {
                        new Claim(ClaimsIdentity.DefaultNameClaimType, streamer),
                        new Claim(ClaimsIdentity.DefaultRoleClaimType, role),
                        new Claim("Moderator", moderator.ModName),
                        new Claim("Scopes", moderator.Scopes)
                    };
                    ClaimsIdentity id = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
                }
            }
            return RedirectToAction("Index", "Home");
        }
    }
}