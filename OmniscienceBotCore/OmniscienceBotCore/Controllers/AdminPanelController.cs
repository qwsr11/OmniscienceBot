﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using OmniscienceBotCore.Channel;
using OmniscienceBotCore.Models;

namespace OmniscienceBotCore.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminPanelController : Controller
    {
        TwitchChannels TwitchChannels;
        public AdminPanelController(TwitchChannels twitchChannels)
        {
            TwitchChannels = twitchChannels;
        }
        public IActionResult Index()
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null)
            {
                return View(TwitchChannels.Channels.Keys.Select(c => new AdminPanelSettings(c)));
            }
            else return RedirectToAction("Index", "Home");
        }
        public async Task<ActionResult> Switch(string channelToSwitch)
        {
            if (channelToSwitch != null && channelToSwitch != "")
            {
                string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
                if (channelToSwitch != channel)
                {
                    await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

                    string role = "Admin";// Roles.GetRole(channel, TwitchChannels.Channels.Keys.ToList());
                    var claims = new List<Claim>
                    {
                        new Claim(ClaimsIdentity.DefaultNameClaimType, channelToSwitch),
                        new Claim(ClaimsIdentity.DefaultRoleClaimType, role)
                    };
                    ClaimsIdentity id = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
                }
            }
            return RedirectToAction("Index", "Home");
        }

        // GET: Pastes/Create
        public ActionResult Create()
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null) return View();
            else return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ChannelSettings channelSettings)
        {
            try
            {
                string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
                if (channel != null) TwitchChannels.AddChannel(channelSettings.Channel);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}