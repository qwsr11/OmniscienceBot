﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OmniscienceBotCore.Models;
using OmniscienceBotCore.Services;

namespace OmniscienceBotCore.Controllers
{
    [Authorize(Roles = "Admin, Streamer")]
    public class InteractionsController : Controller
    {
        TwitchClientWrapper TwitchClientWrapper;
        public InteractionsController(TwitchClientWrapper twitchClientWrapper)
        {
            TwitchClientWrapper = twitchClientWrapper;
        }
        public IActionResult Index()
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel != null) return View();
            else return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(string message)
        {
            if (message != null && message != "")
            {
                string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
                TwitchClientWrapper.SendMessage(channel, message);
            }
            return View();
        }
    }
}