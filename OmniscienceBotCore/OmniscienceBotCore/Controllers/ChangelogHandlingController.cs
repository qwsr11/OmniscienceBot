﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OmniscienceBotCore.Channel;
using OmniscienceBotCore.DataManagement;
using OmniscienceBotCore.Models;
using OmniscienceBotCore.Models.Commands;
using OmniscienceBotCore.Models.Events;
using System.Linq;
using System.Security.Claims;
using static SpotifyAPI.Web.PlaylistRemoveItemsRequest;

namespace OmniscienceBotCore.Controllers
{
    [Authorize(Roles = "Admin, Streamer")]
    public class ChangelogHandlingController : Controller
    {
        TwitchChannels TwitchChannels;
        public ChangelogHandlingController(TwitchChannels twitchChannels)
        {
            TwitchChannels = twitchChannels;
        }

        public IActionResult Changelog(string id, string entityName)
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;

            if (channel != null)
            {
                using (DataContext db = new DataContext())
                {
                    var logs = db.EntitiesChangelog.AsQueryable().Where(log => log.EntityName == entityName && log.EntityId == id).ToList();

                    return View(logs);
                }
            }
            else return RedirectToAction("Index", "Home");
        }

        public IActionResult Restore(int id)
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;

            if (channel != null)
            {
                using (DataContext db = new DataContext())
                {
                    var entityChange = db.EntitiesChangelog.AsQueryable().FirstOrDefault(log => log.Id == id);

                    switch (entityChange.EntityName)
                    {
                        case "Event": TwitchChannels.Channels[channel].Events.Change(JsonConvert.DeserializeObject<Event>(entityChange.EntityNewJson), channel); break;
                        case "TextCommand": TwitchChannels.Channels[channel].Commands.Change(JsonConvert.DeserializeObject<TextCommand>(entityChange.EntityNewJson), channel); break;
                        case "Paste": TwitchChannels.Channels[channel].Pastes.Change(JsonConvert.DeserializeObject<Paste>(entityChange.EntityNewJson), channel); break;
                        case "Counter": TwitchChannels.Channels[channel].Counters.Change(JsonConvert.DeserializeObject<Counter>(entityChange.EntityNewJson), channel); break;
                        case "StreamAnnouncement": TwitchChannels.Channels[channel].Announcements.Change(JsonConvert.DeserializeObject<StreamAnnouncement>(entityChange.EntityNewJson), channel); break;
                    }

                    return RedirectToAction("Changelog", new { id = entityChange.EntityId, entityName = entityChange.EntityName });
                }
            }
            else return RedirectToAction("Index", "Home");
        }

        public IActionResult Remove(int id)
        {
            string channel = User.Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;

            if (channel != null)
            {
                using (DataContext db = new DataContext())
                {
                    var entityChange = db.EntitiesChangelog.AsQueryable().FirstOrDefault(log => log.Id == id);
                    if (entityChange != null)
                    {
                        db.Remove(entityChange);
                        db.SaveChanges();
                    }

                    return RedirectToAction("Changelog", new { id = entityChange.EntityId, entityName = entityChange.EntityName });
                }
            }
            else return RedirectToAction("Index", "Home");
        }
    }
}