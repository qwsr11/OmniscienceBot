﻿using NTextCat;
using System.Linq;
using System.Speech.Synthesis;

namespace OmniscienceBot
{
    class TextToSpeechEntry
    {
        static RankedLanguageIdentifier identifier;
        static TextToSpeechEntry()
        {
            identifier = new RankedLanguageIdentifierFactory().Load("Libs\\LanguageModels\\Core14.profile.xml");
        }

        string Name { get; set; }
        string Text { get; set; }
        public Prompt Prompt { get; set; }
        public TextToSpeechEntry(string name, string text)
        {
            Name = name;
            Text = text;

            PromptBuilder pb = new PromptBuilder();
            pb.StartVoice(GetVoice(text));
            pb.AppendText(text);
            pb.EndVoice();

            Prompt = new Prompt(pb);
        }

        VoiceInfo GetVoice(string text)
        {
            var languages = identifier.Identify(text);
            var mostCertainLanguage = languages.FirstOrDefault();
            var list = Narrator.InstalledVoices;

            if (list.Exists(v => v.VoiceInfo.Culture.ThreeLetterISOLanguageName == mostCertainLanguage.Item1.Iso639_3))
                return list.First(v => v.VoiceInfo.Culture.ThreeLetterISOLanguageName == mostCertainLanguage.Item1.Iso639_3).VoiceInfo;
            else
                return list.Find(v => v.VoiceInfo.Culture.ThreeLetterISOLanguageName == "rus").VoiceInfo;
        }

        public override string ToString()
        {
            return $"{Name}: {Text}";
        }

        public override bool Equals(object obj)
        {
            if (!(obj is TextToSpeechEntry)) return false;
            return ((TextToSpeechEntry)obj).Prompt == Prompt;
        }
    }
}
