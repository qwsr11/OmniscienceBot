﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OmniscienceBot
{
    static class ConditionsFactory
    {
        static string argumentsSeparator = "#";
        static List<ChannelRole> availableRoles = new List<ChannelRole>();
        static Dictionary<string, string> typesMatch = new Dictionary<string, string>();
        static List<string> changeTypes = new List<string>();
        static ConditionsFactory()
        {
            availableRoles.Add(ChannelRole.User);
            availableRoles.Add(ChannelRole.Subscriber);
            availableRoles.Add(ChannelRole.Trustworthy);
            availableRoles.Add(ChannelRole.Moderator);
            availableRoles.Add(ChannelRole.Broadcaster);

            typesMatch.Add("Если все, что ниже - истинно...", "&");
            typesMatch.Add("Если что-нибудь из того, что ниже - истинно...", "|");

            typesMatch.Add("Ник равен:", "1");
            typesMatch.Add("Сообщение совпадает с:", "2");
            typesMatch.Add("Сообщение содержит:", "3");
            typesMatch.Add("Роль:", "4");
            typesMatch.Add("Роль не ниже:", "5");

            changeTypes.AddRange(typesMatch.Keys);
            changeTypes.Add("Удалить");
        }
        static void removeButton_Click(object sender, RoutedEventArgs e)
        {
            StackPanel parent = (sender as Button).Parent as StackPanel;
            (parent.Parent as Panel).Children.Remove(parent);
        }

        private static void creatorCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox creatorCB = sender as ComboBox;

            if (creatorCB.SelectedIndex != -1)
            {
                Panel parent = creatorCB.Tag as Panel;

                switch (typesMatch[e.AddedItems[0].ToString()])
                {
                    case "&":
                        SpawnCreator(CreateAnd(parent));
                        DisposeCreator(creatorCB);
                        break;
                    case "|":
                        SpawnCreator(CreateOr(parent));
                        DisposeCreator(creatorCB);
                        break;
                    case "!": break;
                    case "1":
                        StackPanel sp1 = Create1(parent);
                        if (!((sp1.Parent as StackPanel).Tag.ToString().Contains("&") || (sp1.Parent as StackPanel).Tag.ToString().Contains("|")))
                            DisposeCreator(creatorCB);
                        break;
                    case "2":
                        StackPanel sp2 = Create2(parent);

                        if (!((sp2.Parent as StackPanel).Tag.ToString().Contains("&") || (sp2.Parent as StackPanel).Tag.ToString().Contains("|")))
                            DisposeCreator(creatorCB);
                        break;
                    case "3":

                        StackPanel sp3 = Create3(parent);
                        if (!((sp3.Parent as StackPanel).Tag.ToString().Contains("&") || (sp3.Parent as StackPanel).Tag.ToString().Contains("|")))
                            DisposeCreator(creatorCB);
                        break;
                    case "4":
                        StackPanel sp4 = Create4(parent);
                        if (!((sp4.Parent as StackPanel).Tag.ToString().Contains("&") || (sp4.Parent as StackPanel).Tag.ToString().Contains("|")))
                            DisposeCreator(creatorCB);
                        break;
                    case "5":
                        StackPanel sp5 = Create5(parent);
                        if (!((sp5.Parent as StackPanel).Tag.ToString().Contains("&") || (sp5.Parent as StackPanel).Tag.ToString().Contains("|")))
                            DisposeCreator(creatorCB);
                        break;
                }
                creatorCB.SelectedIndex = -1;
            }

        }

        public static StackPanel SpawnCreator(Panel parent)
        {
            StackPanel sp = new StackPanel();

            ComboBox creatorCB = new ComboBox();
            creatorCB.ItemsSource = typesMatch.Keys;
            creatorCB.Tag = parent;
            creatorCB.SelectionChanged += creatorCB_SelectionChanged;
            sp.Children.Add(creatorCB);

            parent.Children.Add(sp);
            return sp;
        }

        public static void DisposeCreator(ComboBox creatorCB)
        {
            (creatorCB.Parent as Panel).Children.Remove(creatorCB);
        }

        public static StackPanel Create(Panel parent, string type)
        {
            StackPanel sp = null;

            switch (type)
            {
                case "&": sp = CreateAnd(parent); break;
                case "|": sp = CreateOr(parent); break;
                case "!": break;
                case "1": sp = Create1(parent); break;
                case "2": sp = Create2(parent); break;
                case "3": sp = Create3(parent); break;
                case "4": sp = Create4(parent); break;
                case "5": sp = Create5(parent); break;
            }

            return sp;
        }

        static StackPanel CreateAnd(Panel parent)
        {
            StackPanel sp = new StackPanel();
            sp.Margin = new Thickness(10 * Convert.ToInt32(parent.Tag.ToString()), 0, 0, 0);
            sp.Tag = Convert.ToInt32(parent.Tag.ToString()) + 1;

            Button removeButton = new Button
            {
                Content = "X"
            };
            removeButton.Click += removeButton_Click;

            ComboBox typeCB = new ComboBox();
            typeCB.ItemsSource = changeTypes;
            typeCB.SelectedIndex = 0;

            sp.Children.Add(removeButton);
            sp.Children.Add(typeCB);
            parent.Children.Add(sp);
            return sp;
        }

        static StackPanel CreateOr(Panel parent)
        {
            StackPanel sp = new StackPanel();
            sp.Margin = new Thickness(10 * Convert.ToInt32(parent.Tag.ToString()), 0, 0, 0);
            sp.Tag = (int)parent.Tag + 1;

            Button removeButton = new Button
            {
                Content = "X"
            };
            removeButton.Click += removeButton_Click;

            ComboBox typeCB = new ComboBox();
            typeCB.ItemsSource = changeTypes;
            typeCB.SelectedIndex = 1;

            sp.Children.Add(removeButton);
            sp.Children.Add(typeCB);
            parent.Children.Add(sp);
            return sp;
        }
        static StackPanel Create1(Panel parent)
        {
            StackPanel sp = new StackPanel();
            sp.Margin = new Thickness(10 * Convert.ToInt32(parent.Tag.ToString()), 0, 0, 0);
            sp.Orientation = Orientation.Horizontal;

            Button removeButton = new Button
            {
                Content = "X"
            };
            removeButton.Click += removeButton_Click;

            ComboBox typeCB = new ComboBox();
            typeCB.ItemsSource = changeTypes;
            typeCB.SelectedIndex = 2;

            TextBox tb = new TextBox();

            sp.Children.Add(removeButton);
            sp.Children.Add(typeCB);
            sp.Children.Add(tb);
            sp.Tag = $"1";
            parent.Children.Add(sp);
            return sp;
        }

        static StackPanel Create2(Panel parent)
        {
            StackPanel sp = new StackPanel();
            sp.Margin = new Thickness(10 * Convert.ToInt32(parent.Tag.ToString()), 0, 0, 0);
            sp.Orientation = Orientation.Horizontal;

            Button removeButton = new Button
            {
                Content = "X"
            };
            removeButton.Click += removeButton_Click;

            ComboBox typeCB = new ComboBox();
            typeCB.ItemsSource = changeTypes;
            typeCB.SelectedIndex = 3;

            TextBox tb = new TextBox();

            sp.Children.Add(removeButton);
            sp.Children.Add(typeCB);
            sp.Children.Add(tb);
            sp.Tag = $"2";
            parent.Children.Add(sp);
            return sp;
        }

        static StackPanel Create3(Panel parent)
        {
            StackPanel sp = new StackPanel();
            sp.Margin = new Thickness(10 * Convert.ToInt32(parent.Tag.ToString()), 0, 0, 0);
            sp.Orientation = Orientation.Horizontal;

            Button removeButton = new Button
            {
                Content = "X"
            };
            removeButton.Click += removeButton_Click;

            ComboBox typeCB = new ComboBox();
            typeCB.ItemsSource = changeTypes;
            typeCB.SelectedIndex = 4;

            TextBox tb = new TextBox();

            sp.Children.Add(removeButton);
            sp.Children.Add(typeCB);
            sp.Children.Add(tb);
            sp.Tag = $"3";
            parent.Children.Add(sp);
            return sp;
        }

        static StackPanel Create4(Panel parent)
        {
            StackPanel sp = new StackPanel();
            sp.Margin = new Thickness(10 * Convert.ToInt32(parent.Tag.ToString()), 0, 0, 0);
            sp.Orientation = Orientation.Horizontal;

            Button removeButton = new Button
            {
                Content = "X"
            };
            removeButton.Click += removeButton_Click;

            ComboBox typeCB = new ComboBox();
            typeCB.ItemsSource = changeTypes;
            typeCB.SelectedIndex = 5;

            ComboBox cb = new ComboBox
            {
                ItemsSource = availableRoles
            };
            cb.SelectedIndex = 0;

            sp.Children.Add(removeButton);
            sp.Children.Add(typeCB);
            sp.Children.Add(cb);
            sp.Tag = $"4";
            parent.Children.Add(sp);
            return sp;
        }

        static StackPanel Create5(Panel parent)
        {
            StackPanel sp = new StackPanel();
            sp.Margin = new Thickness(10 * Convert.ToInt32(parent.Tag.ToString()), 0, 0, 0);
            sp.Orientation = Orientation.Horizontal;

            Button removeButton = new Button
            {
                Content = "X"
            };
            removeButton.Click += removeButton_Click;

            ComboBox typeCB = new ComboBox();
            typeCB.ItemsSource = changeTypes;
            typeCB.SelectedIndex = 6;

            ComboBox cb = new ComboBox
            {
                ItemsSource = availableRoles
            };
            cb.SelectedIndex = 0;

            sp.Children.Add(removeButton);
            sp.Children.Add(typeCB);
            sp.Children.Add(cb);
            sp.Tag = $"5";
            parent.Children.Add(sp);
            return sp;
        }
    }
}
