﻿using NTextCat;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace OmniscienceBot
{
    class Narrator
    {
        public static List<InstalledVoice> InstalledVoices { get; set; }

        static Narrator()
        {
            InstalledVoices = new List<InstalledVoice>(new SpeechSynthesizer().GetInstalledVoices());
        }

        SpeechSynthesizer synth;
        public ObservableCollection<TextToSpeechEntry> Queue { get; set; }
        public TextToSpeechEntry CurrentT2S { get; set; }

        Dispatcher mwDispatcher;

        public Narrator(Dispatcher dp)
        {
            mwDispatcher = dp;
            synth = new SpeechSynthesizer();
            synth.SpeakCompleted += Synth_SpeakCompleted;
            Queue = new ObservableCollection<TextToSpeechEntry>();
        }

        private void Synth_SpeakCompleted(object sender, SpeakCompletedEventArgs e)
        {
            mwDispatcher.Invoke(new Action(() => Queue.Remove(CurrentT2S)));
            CurrentT2S = null;
            if (Queue.Count != 0)
            {
                CurrentT2S = Queue[0];
                synth.SpeakAsync(CurrentT2S.Prompt);
            }
        }

        public void Pronounce(string name, string text)
        {
            mwDispatcher.Invoke(new Action(() => Queue.Add(new TextToSpeechEntry(name, text))));
            if (CurrentT2S == null)
            {
                CurrentT2S = Queue[0];
                synth.SpeakAsync(CurrentT2S.Prompt);
            }
        }

        public void Skip(TextToSpeechEntry t2s)
        {
            synth.SpeakAsyncCancel(t2s.Prompt);
            mwDispatcher.Invoke(new Action(() => Queue.Remove(t2s)));
            CurrentT2S = null;
        }

        public void SkipAll()
        {
            synth.SpeakAsyncCancelAll();
        }
    }
}
