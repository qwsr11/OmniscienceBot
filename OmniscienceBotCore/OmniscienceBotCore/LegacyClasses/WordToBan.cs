﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OmniscienceBot
{
    class WordToBan
    {
        public string Expression { get; set; }
        public string Response { get; set; }
        public WordToBan(string expr, string resp)
        {
            Expression = expr;
            Response = resp;
        }

        public override bool Equals(object obj)
        {
            return (obj is WordToBan) && (obj as WordToBan).Expression == Expression;
        }
    }
}