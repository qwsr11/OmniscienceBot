﻿using Microsoft.AspNetCore.Mvc;
using OmniscienceBotCore.Channel;
using System.Linq;
using System.Security.Claims;

namespace OmniscienceBotCore.Components
{
    public class Login : ViewComponent
    {
        TwitchChannels TwitchChannels;
        public Login(TwitchChannels twitchChannels)
        {
            TwitchChannels = twitchChannels;
        }

        public IViewComponentResult Invoke()
        {
            string channel = (User as ClaimsPrincipal).Claims.FirstOrDefault(c => c.Type == ClaimsIdentity.DefaultNameClaimType)?.Value;
            if (channel == null) return Content("");
            else return View("Login");
        }
    }
}