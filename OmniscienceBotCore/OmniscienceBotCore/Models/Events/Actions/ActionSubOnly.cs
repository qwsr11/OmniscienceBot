﻿using OmniscienceBotCore.Services;

namespace OmniscienceBotCore.Models.Events.Actions
{
    public class ActionSubOnly : BaseAction
    {
        public ActionSubOnly(TwitchClientWrapper twitchClient, string parameter, int? probability = null, bool group = false) : base(twitchClient, parameter, probability, group)        {        }

        protected override void Perform(MessageParams message)
        {
            TwitchClient.SetSubOnly(message.Channel, Parameter != "");
        }
    }
}