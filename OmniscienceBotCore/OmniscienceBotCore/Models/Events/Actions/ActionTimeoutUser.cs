﻿using OmniscienceBotCore.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Models.Events.Actions
{
    public class ActionTimeoutUser : BaseAction
    {
        public ActionTimeoutUser(TwitchClientWrapper twitchClient, string parameter, int? probability = null, bool group = false) : base(twitchClient, parameter, probability, group) { }

        protected override void Perform(MessageParams message)
        {
            string tempPar = MarkupSubstitution(Parameter, message);
            string nickname = tempPar.Split('#')[0];
            int time = Convert.ToInt32(tempPar.Split('#')[1]);
            TwitchClient.TimeoutById(message.Channel, message.SourceId, time);
        }
    }
}
