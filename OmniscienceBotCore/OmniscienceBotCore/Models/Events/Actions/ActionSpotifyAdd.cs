﻿using OmniscienceBotCore.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Models.Events.Actions
{
    public class ActionSpotifyAdd : BaseAction
    {
        public ActionSpotifyAdd(TwitchClientWrapper twitchClient, string parameter, int? probability = null, bool group = false) : base(twitchClient, parameter, probability, group) { }

        protected override void Perform(MessageParams message)
        {
            var t = MarkupSubstitution(Parameter, message).Split(':').ToList();
            TwitchClient.SpotifyAdd(message.Channel, $"spotify:track:{t[t.LastIndexOf("track") + 1]}");
        }
    }
}
