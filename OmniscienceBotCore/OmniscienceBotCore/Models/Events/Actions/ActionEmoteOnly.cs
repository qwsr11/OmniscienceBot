﻿using OmniscienceBotCore.Services;

namespace OmniscienceBotCore.Models.Events.Actions
{
    public class ActionEmoteOnly : BaseAction
    {
        public ActionEmoteOnly(TwitchClientWrapper twitchClient, string parameter, int? probability = null, bool group = false) : base(twitchClient, parameter, probability, group) { }

        protected override void Perform(MessageParams message)
        {
            TwitchClient.SetEmoteOnly(message.Channel, Parameter != "");
        }
    }
}