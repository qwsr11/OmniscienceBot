﻿using OmniscienceBotCore.Services;
using System;

namespace OmniscienceBotCore.Models.Events.Actions
{
    public class ActionFollowerOnly : BaseAction
    {
        public ActionFollowerOnly(TwitchClientWrapper twitchClient, string parameter, int? probability = null, bool group = false) : base(twitchClient, parameter, probability, group) { }

        protected override void Perform(MessageParams message)
        {
            if (Parameter == "")
                TwitchClient.SetFollowerOnly(message.Channel, Parameter != "");
            else if (int.TryParse(Parameter, out _))
                TwitchClient.SetFollowerOnly(message.Channel, Parameter != "", Convert.ToInt32(Parameter));
        }
    }
}