﻿using OmniscienceBotCore.Services;

namespace OmniscienceBotCore.Models.Events.Actions
{
    public class ActionClearChat : BaseAction
    {
        public ActionClearChat(TwitchClientWrapper twitchClient, string parameter, int? probability = null, bool group = false) : base(twitchClient, parameter, probability, group) { }

        protected override void Perform(MessageParams message)
        {
            TwitchClient.ClearChat(message.Channel);
        }
    }
}