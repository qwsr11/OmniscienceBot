﻿using OmniscienceBotCore.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Models.Events.Actions
{
    public class ActionSendTextInChat : BaseAction
    {
        public ActionSendTextInChat(TwitchClientWrapper twitchClient, string parameter, int? probability = null, bool group = false) : base(twitchClient, parameter, probability, group) { }

        protected override void Perform(MessageParams message)
        {
            TwitchClient.SendMessage(message.Channel, MarkupSubstitution(Parameter, message));
        }
    }
}
