﻿using OmniscienceBotCore.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Models.Events.Actions
{
    public abstract class BaseAction
    {
        protected TwitchClientWrapper TwitchClient;
        protected string Parameter;
        protected int? Probability;
        public bool Group;

        public BaseAction(TwitchClientWrapper twitchClient, string parameter, int? probability = null, bool group = false)
        {
            TwitchClient = twitchClient;
            Parameter = parameter;
            Probability = probability;
            Group = group;
        }

        protected static string MarkupSubstitution(string s, MessageParams parameters)
        {
            s = s.Replace("@source", parameters.Source);
            for (int i = 0; i < parameters.Destinations.Count; i++)
                s = s.Replace($"@dest{i}", parameters.Destinations[i]);
            s = s.Replace("@message", parameters.Message);
            s = s.Replace("@role", parameters.Role.ToString());
            s = s.Replace("@reward", parameters.RewardID);

            return s;
        }
        bool ProbabilityCheck()
        {
            if (Probability != null && new Random().Next(101) >= Probability) return false;
            return true;
        }
        protected abstract void Perform(MessageParams message);
        public bool Invoke(MessageParams message)
        {
            bool res = ProbabilityCheck();
            if (res) Perform(message);

            return res;
        }
        public override string ToString()
        {
            return $"{GetType().Name}->{Parameter}" + (Probability == null ? "" : $" ({Probability}%)");
        }
    }
}
