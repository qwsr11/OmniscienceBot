﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Models.Events.Triggers
{
    public class GroupAnd : Group
    {
        public GroupAnd(List<ITrigger> clauses, bool inverted) : base(clauses, inverted) { }

        public override bool Check(MessageParams message)
        {
            foreach (var clause in Clauses)
                if (!clause.Check(message))
                    return false;
            return true;
        }
    }
}
