﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Models.Events.Triggers
{
    public class OnRewardID : Trigger<string>
    {
        public OnRewardID(string param, bool inverted = false)
        {
            Parameter = param;
            Inverted = inverted;
        }

        public override bool Check(MessageParams message)
        {
            bool res = message.RewardID == Parameter;
            return Inverted ? !res : res;
        }
    }
}
