﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Models.Events.Triggers
{
    public class OnMessageContains : Trigger<string>
    {
        public OnMessageContains(string param, bool inverted = false)
        {
            Parameter = param;
            Inverted = inverted;
        }

        public override bool Check(MessageParams message)
        {
            bool res = message.Message.ToLower().Contains(Parameter.ToLower());
            return Inverted ? !res : res;
        }
    }
}
