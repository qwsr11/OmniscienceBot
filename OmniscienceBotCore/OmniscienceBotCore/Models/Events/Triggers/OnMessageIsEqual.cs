﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Models.Events.Triggers
{
    public class OnMessageIsEqual : Trigger<string>
    {
        public OnMessageIsEqual(string param, bool inverted = false)
        {
            Parameter = param;
            Inverted = inverted;
        }

        public override bool Check(MessageParams message)
        {
            bool res = message.Message.ToLower() == Parameter.ToLower();
            return Inverted ? !res : res;
        }
    }
}
