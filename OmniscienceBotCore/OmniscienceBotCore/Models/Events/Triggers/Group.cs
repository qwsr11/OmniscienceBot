﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Models.Events.Triggers
{
    public abstract class Group : ITrigger
    {
        protected bool Inverted;
        protected List<ITrigger> Clauses;
        protected Group(List<ITrigger> clauses, bool inverted)
        {
            Clauses = clauses;
            Inverted = inverted;
        }

        public abstract bool Check(MessageParams message);
        public override string ToString()
        {
            string res = Inverted ? "!" : "" + GetType().Name;

            if (Clauses != null)
                foreach (var clause in Clauses)
                    res += $"\r\n{clause}";

            return res;
        }
    }
}
