﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Models.Events.Triggers
{
    public abstract class Trigger<ParameterType> : ITrigger
    {
        protected ParameterType Parameter;
        protected bool Inverted;

        protected static readonly Dictionary<string, string> BinaryPattern = new Dictionary<string, string>()
        {
            ["#usernameIsEqual"] = $"@source", //username
            ["#messageIsEqual"] = $"@message", //message is equal to
            ["#messageContains"] = $"@message", //message contains 
            ["#reward"] = $"@reward", //message contains 
            ["#userRole"] = $"@role", // role is equal to
            ["#userRoleHigher"] = $"@role", // role is higher than
        };

        public abstract bool Check(MessageParams message);

        public override string ToString()
        {
            return Inverted ? "!" : "" + $"{GetType().Name}->{Parameter}";
        }
    }
}
