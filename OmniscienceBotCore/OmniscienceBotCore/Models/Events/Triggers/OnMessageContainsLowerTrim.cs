﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Models.Events.Triggers
{
    public class OnMessageContainsLowerTrim : Trigger<string>
    {
        public OnMessageContainsLowerTrim(string param, bool inverted = false)
        {
            Parameter = param;
            Inverted = inverted;
        }

        public override bool Check(MessageParams message)
        {
            bool res = message.Message.Replace(" ", "").ToLower().Contains(Parameter.Replace(" ","").ToLower());
            return Inverted ? !res : res;
        }
    }
}