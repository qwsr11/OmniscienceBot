﻿using OmniscienceBotCore.Models.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Models.Events.Triggers
{
    public class OnUserRoleEqual : Trigger<TwitchChannelRole>
    {
        public OnUserRoleEqual(TwitchChannelRole param, bool inverted = false)
        {
            Parameter = param;
            Inverted = inverted;
        }

        public override bool Check(MessageParams message)
        {
            bool res = message.Role == Parameter;
            return Inverted ? !res : res;
        }
    }
}
