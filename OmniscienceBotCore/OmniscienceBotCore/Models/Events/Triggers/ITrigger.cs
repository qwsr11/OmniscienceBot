﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Models.Events.Triggers
{
    public interface ITrigger
    {
        public abstract bool Check(MessageParams message);
    }
}
