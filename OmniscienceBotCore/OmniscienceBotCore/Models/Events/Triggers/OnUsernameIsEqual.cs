﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Models.Events.Triggers
{
    public class OnUsernameIsEqual : Trigger<string>
    {
        public OnUsernameIsEqual(string param, bool inverted = false)
        {
            Parameter = param;
            Inverted = inverted;
        }

        public override bool Check(MessageParams message)
        {
            bool res = message.Source.ToLower() == Parameter.ToLower();
            return Inverted ? !res : res;
        }
    }
}
