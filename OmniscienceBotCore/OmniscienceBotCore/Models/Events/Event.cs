﻿using Newtonsoft.Json;
using OmniscienceBotCore.Channel;
using OmniscienceBotCore.Models.Commands;
using OmniscienceBotCore.Models.Events.Actions;
using OmniscienceBotCore.Models.Events.Triggers;
using OmniscienceBotCore.Services;
using SQLite;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TwitchLib.Api.Models.Undocumented.ClipChat;
using TwitchLib.Client.Models;
using TwitchLib.PubSub.Events;
using TwitchLib.PubSub.Models.Responses.Messages.Redemption;

namespace OmniscienceBotCore.Models.Events
{
    public class Event : IdModel<int>
    {
        static readonly string probabilityDelimeter = "##";
        TwitchClientWrapper twitchClient;

        public string Channel { get; set; }
        [NotMapped, JsonIgnore]
        public bool IsPublic => Channel == "";

        public IList<ChannelsGlobalEvents> ChannelsGlobalEvents { get; set; }
        public bool Active { get; set; }
        [NotMapped, JsonIgnore]
        public ITrigger Condition { get; set; }
        [NotMapped, JsonIgnore]
        public List<BaseAction> ActionsList { get; set; }
        public string Json { get; set; }

        public Event() { }
        public Event(string channel, TwitchClientWrapper twitchClient, string json, bool active)
        {
            Channel = channel;

            Json = json;
            Init(twitchClient);

            Active = active;
        }

        public void Init(TwitchClientWrapper twitchClient) => Init(twitchClient, Data.FromJson(Json));
        void Init(TwitchClientWrapper twitchClient, Data data)
        {
            this.twitchClient = twitchClient;
            Condition = GetTrigger(data);
            ActionsList = GetActions(data.Options.Methods);
        }

        #region Execution
        bool Check(MessageParams parameters) => Condition.Check(parameters);
        void Process(MessageParams parameters)
        {
            foreach (var action in ActionsList.Where(a => a.Group))
                if (action.Invoke(parameters))
                    break;
            foreach (var action in ActionsList.Where(a => !a.Group))
                action.Invoke(parameters);
        }

        public bool Execute(MessageParams parameters)
        {
            if (Check(parameters))
            {
                Process(parameters);
                return true;
            }
            return false;
        }
        public bool Execute(ChatMessage chatMessage, TwitchChannelRole role) => Execute(new MessageParams(chatMessage, role));
        public bool Execute(OnRewardRedeemedArgs reward, string channel, TwitchChannelRole role) => Execute(new MessageParams(reward, channel, role));

        #endregion

        #region Triggers extraction
        ITrigger GetTrigger(Data data)
        {
            return Itterate(data.Options.Conditions);
        }

        ITrigger Itterate(Conditions condition)
        {
            if (condition.Name == null) return null;
            switch (condition.Model.Type)
            {
                //case TypeEnum.Group: return $" ({ItterateSubs(condition.Model.Subs)}#{condition.Name})";
                case TypeEnum.Group: return GetGroup(condition);
                case TypeEnum.Input:
                case TypeEnum.Select: return GetSingle(condition);

                default: return null;
            }
        }

        ITrigger GetSingle(Conditions condition)
        {
            switch (condition.Name)
            {
                case "usernameIsEqual": return new OnUsernameIsEqual(condition.Model.Value, condition.Model.Inversion == true);
                case "messageIsEqual": return new OnMessageIsEqual(condition.Model.Value, condition.Model.Inversion == true);
                case "messageContains": return new OnMessageContains(condition.Model.Value, condition.Model.Inversion == true);
                case "messageContainsLowerTrim": return new OnMessageContainsLowerTrim(condition.Model.Value, condition.Model.Inversion == true);
                case "reward": return new OnRewardID(condition.Model.Value, condition.Model.Inversion == true);
                case "userRole": return new OnUserRoleEqual(GetChannelRole(condition.Model.Value), condition.Model.Inversion == true);
                case "userRoleHigher": return new OnUserRoleHigher(GetChannelRole(condition.Model.Value), condition.Model.Inversion == true);
                default: return null;
            }
        }

        TwitchChannelRole GetChannelRole(string role)
        {
            switch (role)
            {
                case "Follower": return TwitchChannelRole.Follower;
                case "Subscriber": return TwitchChannelRole.Subscriber;
                case "VIP": return TwitchChannelRole.VIP;
                case "Trustworthy": return TwitchChannelRole.Trustworthy;
                case "Moderator": return TwitchChannelRole.Moderator;
                case "Broadcaster": return TwitchChannelRole.Broadcaster;
                default: return TwitchChannelRole.User;
            }
        }

        ITrigger GetGroup(Conditions condition)
        {
            switch (condition.Name)
            {
                case "groupAnd": return new GroupAnd(ItterateSubs(condition.Model.Subs), condition.Model.Inversion == true);
                case "groupOr": return new GroupOr(ItterateSubs(condition.Model.Subs), condition.Model.Inversion == true);
                default: return null;
            }
        }

        List<ITrigger> ItterateSubs(List<Conditions> conditions)
        {
            List<ITrigger> res = new List<ITrigger>();
            foreach (var condition in conditions)
                if (condition.Model.Type != null)
                    res.Add(Itterate(condition));

            return res;
        }
        #endregion

        #region Actions extraction
        List<BaseAction> GetActions(List<Method> methods)
        {
            List<BaseAction> res = new List<BaseAction>();

            foreach (Method method in methods)
                if (method.Name != null && method.Name != "empty")
                {
                    //string key = method.Name;
                    string value = method.Model.Value;
                    int? probability = null;
                    bool group = false;
                    if (method.Model.Value.Split(probabilityDelimeter).Length == 2)
                    {
                        string[] act = method.Model.Value.Split(probabilityDelimeter);
                        value = act[0];
                        if (act[1].Contains('!'))
                        {
                            act[1] = act[1].Remove(act[1].Length - 1);
                            group = true;
                        }
                        probability = Convert.ToInt32(act[1]);
                    }

                    BaseAction baseAction;
                    switch (method.Name)
                    {
                        case "sendTextInChat":
                            baseAction = new ActionSendTextInChat(twitchClient, value, probability, group); break;
                        case "deleteMessage":
                            baseAction = new ActionDeleteMessage(twitchClient, value, probability, group); break;
                        case "timeoutUser":
                            baseAction = new ActionTimeoutUser(twitchClient, value, probability, group); break;
                        case "banUser":
                            baseAction = new ActionBanUser(twitchClient, value, probability, group); break;
                        case "clearChat":
                            baseAction = new ActionClearChat(twitchClient, value, probability, group); break;
                        case "followerOnly":
                            baseAction = new ActionFollowerOnly(twitchClient, value, probability, group); break;
                        case "subOnly":
                            baseAction = new ActionSubOnly(twitchClient, value, probability, group); break;
                        case "emoteOnly":
                            baseAction = new ActionEmoteOnly(twitchClient, value, probability, group); break;
                        case "spotifyPlay":
                            baseAction = new ActionSpotifyPlay(twitchClient, value, probability, group); break;
                        case "spotifyPause":
                            baseAction = new ActionSpotifyPause(twitchClient, value, probability, group); break;
                        case "spotifySkip":
                            baseAction = new ActionSpotifySkip(twitchClient, value, probability, group); break;
                        case "spotifyAdd":
                            baseAction = new ActionSpotifyAdd(twitchClient, value, probability, group); break;
                        default:
                            throw new NotImplementedException();
                    }
                    res.Add(baseAction);
                }

            return res;
        }
        #endregion





        #region Operators and actions
        public static readonly Dictionary<string, Func<object, bool>> UnaryOperators = new Dictionary<string, Func<object, bool>>()
        {
            ["#inversion"] = (arg) => { return !(bool)arg; }
        };
        public static readonly Dictionary<string, Func<List<bool>, bool>> LogicOperators = new Dictionary<string, Func<List<bool>, bool>>()
        {
            ["#groupAnd"] = (args) =>
            {
                for (int i = 0; i < args.Count; i++)
                    if (!args[i])
                        return false;
                return true;
            },
            ["#groupOr"] = (args) =>
            {
                for (int i = 0; i < args.Count; i++)
                    if (args[i])
                        return true;
                return false;
            }
        };
        public static readonly Dictionary<string, Func<object, object, bool>> BinaryOperators = new Dictionary<string, Func<object, object, bool>>()
        {
            ["#usernameIsEqual"] = (arg1, arg2) => { return arg1.ToString().ToLower() == arg2.ToString().ToLower(); }, //username
            ["#messageIsEqual"] = (arg1, arg2) => { return arg1.ToString().ToLower() == arg2.ToString().ToLower(); }, //message is equal to
            ["#messageContains"] = (arg1, arg2) => { return arg2.ToString().ToLower().Contains(arg1.ToString().ToLower()); }, //message contains 
            ["#reward"] = (arg1, arg2) => { return arg1 == null ? false : arg1.ToString().ToLower() == arg2.ToString().ToLower(); }, //message is equal to
            ["#userRole"] = (arg1, arg2) => { return (TwitchChannelRole)Enum.Parse(typeof(TwitchChannelRole), arg1.ToString()) == (TwitchChannelRole)Enum.Parse(typeof(TwitchChannelRole), arg2.ToString()); }, // role is equal to
            ["#userRoleHigher"] = (arg2, arg1) => { return (TwitchChannelRole)Enum.Parse(typeof(TwitchChannelRole), arg1.ToString()) >= (TwitchChannelRole)Enum.Parse(typeof(TwitchChannelRole), arg2.ToString()); }, // role is higher than
        };
        public static readonly Dictionary<string, string> BinaryPattern = new Dictionary<string, string>()
        {
            ["#usernameIsEqual"] = $"@source", //username
            ["#messageIsEqual"] = $"@message", //message is equal to
            ["#messageContains"] = $"@message", //message contains 
            ["#reward"] = $"@reward", //message contains 
            ["#userRole"] = $"@role", // role is equal to
            ["#userRoleHigher"] = $"@role", // role is higher than
        };
        public static readonly Dictionary<string, Action<TwitchClientWrapper, MessageParams, string>> Actions = new Dictionary<string, Action<TwitchClientWrapper, MessageParams, string>>()
        {
            ["sendTextInChat"] = (twitchClientWrapper, parameters, message) => { twitchClientWrapper.SendMessage(parameters.Channel, message); }, // send a message
            ["playSound"] = (twitchClientWrapper, channel, command) => {; }, // execute a command
            ["deleteMessage"] = (twitchClientWrapper, parameters, command) => { twitchClientWrapper.DeleteChatMessage(parameters.Channel, parameters.MessageID); },
            ["timeoutUser"] = (twitchClientWrapper, parameters, data) => { twitchClientWrapper.Timeout(parameters.Channel, data.Split('#')[0], Convert.ToInt32(data.Split('#')[1])); }, // timeout
            ["banUser"] = (twitchClientWrapper, parameters, username) => { twitchClientWrapper.Ban(parameters.Channel, username); }, // ban
        };
        private static readonly Dictionary<string, (string symbol, int precedence, bool rightAssociative)> operators = new (string symbol, int precedence, bool rightAssociative)[] {
            ("#inversion", 4, true),
            ("#usernameIsEqual", 3, false),
            ("#messageIsEqual", 3, false),
            ("#messageContains", 3, false),
            ("#userRole", 3, false),
            ("#userRoleHigher", 3, false),
            ("#reward", 3, false),
            ("#groupAnd", 2, false),
            ("#groupOr", 2, false)
}.ToDictionary(op => op.symbol);
        #endregion

        #region Validation
        public static string Validate(EventsConstructorWrapper json)
        {
            if (json.Data.Options.Methods == null) return "No methods provided";
            foreach (Method method in json.Data.Options.Methods.Where(m => m.Model != null))
            {
                if (method.Model.Value == "") return "Empty field provided";
                if (!ValidateProbabilty(method.Model.Value)) return "Invalid probablity format";
                switch (method.Name)
                {
                    case "timeoutUser":
                        if (!ValidateTimeout(method.Model.Value)) return "User timeout method has invalid parameters";
                        else break;
                }
            }

            return "OK";
        }
        static bool ValidateProbabilty(string value)
        {
            int parameters = value.Split(probabilityDelimeter).Length;
            if (parameters == 1) return true;
            if (parameters == 2)
            {
                string s = value.Split(probabilityDelimeter)[1];
                if (s.Last() == '!') s = s.Remove(s.Length - 1);
                if (int.TryParse(s, out int p))
                {
                    if (!(p >= 0 && p <= 100))
                        return false;
                }
                else return false;
            }

            return true;
        }
        static bool ValidateTimeout(string value)
        {
            int parameters = value.Split('#').Length;
            if (!(parameters >= 2 && int.TryParse(value.Split('#')[1], out int t))) return false;

            return true;
        }
        #endregion

    }
}