﻿using OmniscienceBotCore.Services;
using SQLite;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Models.Events
{
    public class ChannelsGlobalEvents
    {
        [ForeignKey(typeof(Event))]
        public int Id { get; set; }
        public Event Event { get; set; }
        [ForeignKey(typeof(ChannelSettings))]
        public string Channel { get; set; }
        public ChannelSettings ChannelSettings { get; set; }
        public int Subscribed { get; set; }
    }
}
