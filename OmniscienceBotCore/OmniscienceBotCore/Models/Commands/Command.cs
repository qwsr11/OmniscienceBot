﻿using OmniscienceBotCore.Channel;
using System.ComponentModel.DataAnnotations;

namespace OmniscienceBotCore.Models.Commands
{
    public abstract class Command : IdModel<int>
    {
        public string Channel { get; set; }
        public string Key { get; set; }
        public string CommandType { get { return GetType().Name; } }
        protected ChannelCommands scs { get; set; }

        protected Command() { }
        protected Command(string channel, string key, ChannelCommands scs)
        {
            Channel = channel;
            Key = key;
            this.scs = scs;
        }

        public abstract object Execute(params object[] args);

        public override string ToString()
        {
            return Key;
        }
    }
}