﻿using OmniscienceBotCore.Channel;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OmniscienceBotCore.Models.Commands
{
    public enum TwitchChannelRole
    {
        User,
        Follower,
        Subscriber,
        VIP,
        Trustworthy,
        Moderator,
        Broadcaster
    }

    public enum CommandHandler
    {
        Stream,
        Discord,
        Both
    }

    public class TextCommand : Command
    {
        public TextCommand() { }

        public TextCommand(string channel, string key, ChannelCommands channelCommands, string text, int period, TwitchChannelRole role, string channelName = null) : base(channel, key, channelCommands)
        {
            Text = text;
            Period = period;
            Role = role;
            ChannelName = channelName;
        }

        public string Text { get; set; }
        public CommandHandler CommandHandler { get; set; }

        public TwitchChannelRole Role { get; set; }
        public int Period { get; set; }
        public bool Periodic => Period != -1;
        [NotMapped]
        public DateTime LastPeriodicExecute { get; set; } = DateTime.MinValue;

        public string ChannelName { get; set; }


        public bool CanExecute(TwitchChannelRole cr) => cr >= Role;
        public bool CanExecute(string channelName) => ChannelName == null || ChannelName.Contains(channelName);
        bool CanExecute(object o) => (o is TwitchChannelRole role && CanExecute(role)) || (o is string channel && CanExecute(channel));
        public override object Execute(params object[] args)
        {
            if (args.Length > 1) return null;
            if (!CanExecute(args[0])) return null;
            LastPeriodicExecute = DateTime.Now;
            return Text;
        }
    }
}