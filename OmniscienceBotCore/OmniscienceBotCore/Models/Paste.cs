﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Models
{
    public class Paste : IdModel<int>
    {
        public string Channel { get; set; }
        public string Key { get; set; }
        public string Text { get; set; }
        public Paste() { }
        public Paste(string channel, string key, string text = "")
        {
            Channel = channel;
            Key = key;
            Text = text;
        }

        public override string ToString()
        {
            return Key;
        }
    }
}