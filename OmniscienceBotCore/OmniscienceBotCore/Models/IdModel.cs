﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Models
{
    public abstract class IdModel<idType>
    {
        public idType Id { get; set; }

        public override bool Equals(object obj) => obj is IdModel<idType> model && Id.Equals(model.Id);
    }
}