﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Models
{
    public class StreamAnnouncement : IdModel<int>
    {
        public string Channel { get; set; }
        public string StreamToAnnounce { get; set; }
        public string DiscordChannel { get; set; }
        public string Text { get; set; }
        public int GracePeriod { get; set; }
        public DateTime LastTimeStopped => lastTimeStopped;
        DateTime lastTimeStopped = DateTime.UtcNow.Subtract(TimeSpan.FromHours(1));

        public StreamAnnouncement()
        {
        }

        public StreamAnnouncement(string channel, string streamToAnnounce, string discordChannel, string text, int gracePeriod = 0)
        {
            Channel = channel;
            StreamToAnnounce = streamToAnnounce;
            DiscordChannel = discordChannel;
            Text = text;
            GracePeriod = gracePeriod;
        }

        public bool CanExecute(DateTime startedAt) => (startedAt - lastTimeStopped).TotalMinutes >= GracePeriod;

        public string Execute(DateTime startedAt) => CanExecute(startedAt) ? Text : null;

        public void Stop(DateTime stop) => lastTimeStopped = stop;
    }
}
