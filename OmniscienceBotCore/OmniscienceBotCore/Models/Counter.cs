﻿using OmniscienceBotCore.Models.Commands;
using OmniscienceBotCore.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TwitchLib.Client.Models;

namespace OmniscienceBotCore.Models
{
    public enum ResponseMode
    {
        OnMessage,
        //EveryNthMessage,
    }
    public enum HandleMode
    {
        Contains,
        Equals,
    }
    public class Counter : IdModel<int>
    {
        public string Channel { get; set; }
        public HandleMode HandleMode { get; set; }
        public string TextToFind { get; set; }
        public ResponseMode ResponseMode { get; set; }
        public string ResponseParams { get; set; }
        public string AnswerPattern { get; set; }
        public int Count { get; set; }

        public Counter() { }
        public Counter(string channel, int id, HandleMode handleMode, string text, ResponseMode responseMode, string responseParams, string answer)
        {
            Channel = channel;
            Id = id;
            TextToFind = text;
            ResponseMode = responseMode;
            HandleMode = handleMode;
            ResponseParams = responseParams;
            AnswerPattern = answer;
            Count = 0;
        }

        bool Check(MessageParams parameters)
        {
            if (HandleMode == OmniscienceBotCore.Models.HandleMode.Contains)
                return parameters.Message.Contains(TextToFind);
            if (HandleMode == OmniscienceBotCore.Models.HandleMode.Equals)
                return parameters.Message == TextToFind;

            return false;
        }
        void Process(TwitchClientWrapper twitchClientWrapper, MessageParams parameters)
        {
            twitchClientWrapper.SendMessage(parameters.Channel, MarkUpSubstitution(AnswerPattern, parameters));
        }
        public object Execute(TwitchClientWrapper twitchClientWrapper, ChatMessage chatMessage, TwitchChannelRole role)
        {
            MessageParams parameters = new MessageParams(chatMessage, role);

            if (Check(parameters))
            {
                Count++;
                Process(twitchClientWrapper, parameters);
                return true;
            }

            return false;
        }
        string MarkUpSubstitution(string s, MessageParams parameters)
        {
            s = s.Replace("@source", parameters.Source);
            for (int i = 0; i < parameters.Destinations.Count; i++)
                s = s.Replace($"@dest{i}", parameters.Destinations[i]);
            s = s.Replace("@message", parameters.Message);
            s = s.Replace("@role", parameters.Role.ToString());
            s = s.Replace("@reward", parameters.RewardID);
            s = s.Replace("@count", Count.ToString());

            return s;
        }
    }
}
