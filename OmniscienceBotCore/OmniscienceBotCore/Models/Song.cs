﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Models
{
    [NotMapped]
    public class Song
    {
        public string ID { get; set; }

        public string Username { get; set; }
        public string Title { get; set; }
        public int Length { get; set; }

        public Song(string username, string id, string title)
        {
            Username = username;
            ID = id;
            Title = title;
        }

        int GetLength(string url)
        {
            url = "https://www.youtube.com/v/h-mUGj41hWA";
            int res = 0;

            HttpWebRequest myRequest = (HttpWebRequest)HttpWebRequest.Create(url);
            HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
            StreamReader sr = new StreamReader(myResponse.GetResponseStream(), Encoding.UTF8);
            string html = sr.ReadToEnd();
            sr.Close();

            Regex regex = new Regex(@"(https://i.ytimg.com/vi/)(?<url>\S*)(/)");
            Match match = regex.Match(html);

            return res;
        }

        public override string ToString()
        {
            return Username + " " + Title;
        }
    }

}
