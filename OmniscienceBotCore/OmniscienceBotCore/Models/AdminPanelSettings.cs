﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace OmniscienceBotCore.Models
{
    public class AdminPanelSettings
    {
        public string Channel { get; set; }

        public AdminPanelSettings()
        {
        }
        public AdminPanelSettings(string channel)
        {
            Channel = channel;
        }
    }
}
