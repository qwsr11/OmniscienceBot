﻿using OmniscienceBotCore.Models.Events;
using SQLiteNetExtensions.Attributes;
using System;

namespace OmniscienceBotCore.Models
{
    public class Moderator : IdModel<int>
    {
        [ForeignKey(typeof(ChannelSettings))]
        public string Channel { get; set; }
        public ChannelSettings Streamer { get; set; }

        [ForeignKey(typeof(ChannelSettings))]
        public string ModName { get; set; }
        public ChannelSettings Mod { get; set; }

        public string Scopes { get; set; }
    }
}