﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using TwitchLib.Client.Events;

namespace OmniscienceBotCore.Models
{
    public class ModeratorsViewModel
    {
        public static readonly List<string> AvailableScopes = new List<string>() { "Events", "Commands", "Pastes", "Counters", "StreamAnnouncements" };

        public string Channel { get; set; }
        public string ModName { get; set; }

        public List<SelectListItem> Scopes { get; set; }

        public IEnumerable<string> SelectedScopes { get; set; }

        public ModeratorsViewModel()
        {
        }

        public ModeratorsViewModel(string channel, string modName = "", List<string> scopes = null)
        {
            Channel = channel;
            ModName = modName;

            Scopes = new List<SelectListItem>();
            foreach (var scope in AvailableScopes)
                Scopes.Add(new SelectListItem(
                    scope,
                    scope,
                    scopes != null && scopes.Contains(scope)));
        }
    }
}