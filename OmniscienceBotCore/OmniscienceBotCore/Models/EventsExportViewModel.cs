﻿using OmniscienceBotCore.Models.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Models
{
    public class EventsExportViewModel
    {
        public string Json { get; set; }
        public EventsExportViewModel() { }
        public EventsExportViewModel(string json)
        {
            Json = json;
        }
    }
}