﻿using OmniscienceBotCore.Models.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Models
{
    public class EventsViewModel
    {
        string channelIssued;
        Event Event { get; set; }
        public int Id { get => Event.Id; }
        public string Conditions { get => Event.Condition == null ? "" : Event.Condition.ToString(); }
        public string Actions { get => (Event.ActionsList == null || Event.ActionsList.Count == 0) ? "" : string.Join("\r\n", Event.ActionsList); }
        public bool Subscribed => Event.ChannelsGlobalEvents != null && Event.ChannelsGlobalEvents.ToList().Exists(c => c.Channel == channelIssued && c.Subscribed == 1);
        public bool Active => Event.Active;
        public EventsViewModel(Event e, string channel)
        {
            Event = e;
            channelIssued = channel;
        }
    }
}