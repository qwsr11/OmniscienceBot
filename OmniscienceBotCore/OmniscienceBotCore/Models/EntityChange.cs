﻿namespace OmniscienceBotCore.Models
{
    public class EntityChange : IdModel<int>
    {
        public string Channel { get; set; }
        public string Editor { get; set; }
        public string Date { get; set; }
        public string EntityName { get; set; }
        public string EntityId { get; set; }
        public string EntityNewJson { get; set; }

        public EntityChange()
        {
        }

        public EntityChange(string channel, string editor, string date, string entityName, string entityId, string entityNewJson)
        {
            Channel = channel;
            Editor = editor;
            Date = date;
            EntityName = entityName;
            EntityId = entityId;
            EntityNewJson = entityNewJson;
        }
    }
}