﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Models
{
    public class DiscordLiveAnnouncment
    {
        public string Channel { get; set; }
        public string DiscordServer { get; set; }
        public string DiscordChannel { get; set; }
        public string OnOnline { get; set; }
        public string? OnOffline { get; set; }

        public DiscordLiveAnnouncment()
        {
        }

        public DiscordLiveAnnouncment(string channel, string discordServer, string discordChannel, string onOnline, string onOffline = null)
        {
            Channel = channel;
            DiscordServer = discordServer;
            DiscordChannel = discordChannel;
            OnOnline = onOnline;
            OnOffline = onOffline;
        }
    }
}