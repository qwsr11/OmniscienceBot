﻿using OmniscienceBot;
using OmniscienceBotCore.Models.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TwitchLib.Client.Models;
using TwitchLib.PubSub.Events;
using TwitchLib.PubSub.Models.Responses.Messages.Redemption;

namespace OmniscienceBotCore.Models
{
    public class MessageParams
    {
        public string Channel { get; set; }
        public string Source { get; set; }
        public string SourceId { get; set; }
        public List<string> Destinations { get; set; }
        public string RewardID { get; set; }
        public string Message { get; set; }
        public string MessageID { get; set; }
        public TwitchChannelRole Role { get; set; }

        public MessageParams(ChatMessage chatMessage, TwitchChannelRole role)
        {
            Channel = chatMessage.Channel;
            Destinations = new List<string>();
            Source = chatMessage.DisplayName;
            SourceId = chatMessage.UserId;
            Message = chatMessage.Message;
            MessageID = chatMessage.Id;
            Role = role;

            Regex regex = new Regex(@"@[\w|\d]{4,25}", RegexOptions.Singleline);
            if (regex.IsMatch(chatMessage.Message))
                Destinations = regex.Matches(chatMessage.Message).Select(m => m.Value).ToList();
        }

        public MessageParams(OnRewardRedeemedArgs reward, string channel, TwitchChannelRole role)
        {
            Channel = channel;
            Destinations = new List<string>();
            Source = reward.DisplayName;
            SourceId = reward.Login;
            Message = reward.Message == null ? "" : reward.Message;
            Role = role;
            RewardID = reward.RewardId.ToString();

            Regex regex = new Regex(@"@[\w|\d]{4,25}", RegexOptions.Singleline);

            if (reward.Message != null)
                if (regex.IsMatch(reward.Message))
                    Destinations = regex.Matches(reward.Message).Select(m => m.Value).ToList();
        }

        public object this[string field] => field switch
        {
            nameof(Channel) => Channel,
            nameof(Source) => Source,
            nameof(Destinations) => Destinations,
            nameof(RewardID) => RewardID,
            nameof(Message) => Message,
            nameof(Role) => Role,
            _ => null,
        };
    }
}