using System;

namespace OmniscienceBotCore.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }
        public string Channel { get; set; }
        public string Message { get; set; }
        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}
