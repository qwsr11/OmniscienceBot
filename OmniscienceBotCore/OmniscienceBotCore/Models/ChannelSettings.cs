﻿using NuGet.Protocol;
using OmniscienceBotCore.Models.Events;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Models
{
    public class SettingsMain
    {
        public string Channel { get; set; }
        public string DiscordServerName { get; set; }
        public bool Active { get; set; }
        public bool SilenceMode { get; set; }
        public string FollowReactionFormat { get; set; }
        public string NewSubReactionFormat { get; set; }
        public string ReSubReactionFormat { get; set; }
        public string GiftedSubReactionFormat { get; set; }
        public string SpotifyRefreshToken { get; set; }
        public string PredictionStartFormat { get; set; }
        public string PredictionCancelFormat { get; set; }
        public int CancelFollowerModeOnRaid { get; set; } = 0;
        public int ShoutoutOnRaid { get; set; } = 0;

        public bool IsStreamer { get; set; } = true;

        public SettingsMain() { }
    }

    public class ChannelSettings
    {
        [Key]
        public string Channel { get; set; }

        public string RawJson { get; set; }

        public ChannelSettings()
        {
        }

        public ChannelSettings(string channel, string rawJson)
        {
            Channel = channel;
            RawJson = rawJson;
        }

        public IList<ChannelsGlobalEvents> ChannelsGlobalEvents { get; set; }

        public IList<Moderator> ChannelModerators { get; set; }

        public IList<Moderator> ModeratedChannels { get; set; }
    }
}