﻿//using OmniscienceBotCore.Services;
//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using TwitchLib.Client.Models;

//namespace OmniscienceBotCore.Models
//{
//    public class DeprecatedEvent1
//    {
//        static string ProbabilityDelimeter = "##";
        
//        class EventAction
//        {
//            public string Key { get; set; }
//            public string Value { get; set; }
//            public int? Probability { get; set; }
//            public bool Group { get; set; }
//        }
//        public string Key { get; set; }
//        public bool Active { get; set; }
//        public List<string> ActionsRaw { get; set; }
//        List<EventAction> ActionsList => ExtractActions(ActionsRaw);
//        public string RPNFormula { get; set; }
//        public string Raw { get; set; }
//        public string Json { get; set; }

//        public static Tuple<Data, string> Config { get; set; }
//        static DeprecatedEvent1()
//        {
//            Data data;
//            using (StreamReader sr = new StreamReader("./wwwroot/api/config.json"))
//                data = EventsJson.FromJsonDataFull(sr.ReadToEnd());
//            EventsJson json = new EventsJson() { Data = data, Status = "success", Msg = "" };
//            Config = new Tuple<Data, string>(data, json.ToJson());
//        }
//        public DeprecatedEvent1() { }
//        public DeprecatedEvent1(string key, string raw, List<string> actionsRaw, string json, bool active)
//        {
//            Key = key;
//            Raw = raw;
//            ActionsRaw = actionsRaw;
//            Json = json;
//            Active = active;
//        }
//        List<EventAction> ExtractActions(List<string> actionsRaw)
//        {
//            List<EventAction> res = new List<EventAction>();

//            foreach (var action in actionsRaw)
//            {
//                string[] s = action.Split("->", StringSplitOptions.RemoveEmptyEntries);
//                string key = s[0];
//                string value;
//                int? probability;
//                bool group = false;
//                if (s[1].Split(ProbabilityDelimeter).Length == 2)
//                {
//                    string[] act = s[1].Split(ProbabilityDelimeter);
//                    value = act[0];
//                    if (act[1].Contains('!'))
//                    {
//                        act[1] = act[1].Remove(act[1].Length - 1);
//                        group = true;
//                    }
//                    probability = Convert.ToInt32(act[1]);
//                }
//                else
//                {
//                    value = s[1];
//                    probability = null;
//                }

//                res.Add(new EventAction() { Key = key, Value = value, Probability = probability, Group = group });
//            }

//            return res;
//        }
//        string GetRPN(string s)
//        {
//            string[] tokens = s.Split("@@", StringSplitOptions.RemoveEmptyEntries);
//            var stack = new Stack<string>();
//            var output = new List<string>();
//            foreach (string token in tokens)
//            {
//                if (operators.TryGetValue(token, out var op1))
//                {
//                    while (stack.Count > 0 && operators.TryGetValue(stack.Peek(), out var op2))
//                    {
//                        int c = op1.precedence.CompareTo(op2.precedence);
//                        if (c < 0 || !op1.rightAssociative && c <= 0) output.Add(stack.Pop());
//                        else break;
//                    }
//                    stack.Push(token);
//                }
//                else if (token == "(")
//                    stack.Push(token);
//                else if (token == ")")
//                {
//                    string top = "";
//                    while (stack.Count > 0 && (top = stack.Pop()) != "(") output.Add(top);
//                    if (top != "(") throw new ArgumentException("No matching left parenthesis.");
//                }
//                else output.Add(token);
//            }
//            while (stack.Count > 0)
//            {
//                var top = stack.Pop();
//                if (!operators.ContainsKey(top)) throw new ArgumentException("No matching right parenthesis.");
//                output.Add(top);
//            }
//            return string.Join("@@", output);
//        }
//        bool Check(MessageParams parameters)
//        {
//            string[] rpnTokens = MarkUpSubstitution(GetRPN(Raw), parameters).Split("@@", StringSplitOptions.RemoveEmptyEntries);
//            Stack<object> stack = new Stack<object>();

//            foreach (object token in rpnTokens)
//            {
//                if (!(UnaryOperators.ContainsKey(token.ToString()) || BinaryOperators.ContainsKey(token.ToString()) || LogicOperators.ContainsKey(token.ToString())))
//                    stack.Push(token);
//                else
//                {
//                    if (UnaryOperators.ContainsKey(token.ToString()))
//                        stack.Push(UnaryOperators[token.ToString()](stack.Pop()));

//                    if (BinaryOperators.ContainsKey(token.ToString()))
//                        stack.Push(BinaryOperators[token.ToString()](stack.Pop(), stack.Pop()));

//                    if (LogicOperators.ContainsKey(token.ToString()))
//                    {
//                        stack.Push(LogicOperators[token.ToString()](new List<bool>() { (bool)stack.Pop(), (bool)stack.Pop() }));
//                        //List<bool> operands = new List<bool>();
//                        ////stack is empty
//                        //while (stack.Count != 0 && !(UnaryOperators.ContainsKey(stack.Peek().ToString()) || BinaryOperators.ContainsKey(stack.Peek().ToString()) || LogicOperators.ContainsKey(stack.Peek().ToString())))
//                        //    operands.Add((bool)stack.Pop());

//                        //stack.Push(LogicOperators[token.ToString()](operands));
//                    }
//                }
//            }
//            return (bool)stack.Pop();
//        }
//        void Process(TwitchClientWrapper twitchClientWrapper, MessageParams parameters)
//        {
//            List<EventAction> groupedActions = new List<EventAction>();
//            foreach (var action in ActionsList)
//                if (Actions.ContainsKey(action.Key))
//                    if (action.Probability != null)
//                    {
//                        if (action.Group) groupedActions.Add(action);
//                        else if (new Random().Next(101) < action.Probability) Actions[action.Key](twitchClientWrapper, parameters, MarkUpSubstitution(action.Value, parameters));
//                    }
//                    else Actions[action.Key](twitchClientWrapper, parameters, MarkUpSubstitution(action.Value, parameters));
//            int roll = new Random().Next(101);
//            foreach (var action in groupedActions)
//            {
//                roll -= (int)action.Probability;
//                if (roll < 0)
//                {
//                    Actions[action.Key](twitchClientWrapper, parameters, MarkUpSubstitution(action.Value, parameters));
//                    break;
//                }
//            }
//        }
//        public object Execute(TwitchClientWrapper twitchClientWrapper, ChatMessage chatMessage, ChannelRole role)
//        {
//            MessageParams parameters = new MessageParams(chatMessage, role);

//            if (Check(parameters))
//            {
//                Process(twitchClientWrapper, parameters);
//                return true;
//            }

//            return false;
//        }
//        public override string ToString()
//        {
//            return Key;
//        }
//        public override bool Equals(object obj)
//        {
//            if (!(obj.GetType() == GetType())) return false;
//            return ((DeprecatedEvent1)obj).Key == Key;
//        }
//        static string MarkUpSubstitution(string s, MessageParams parameters)
//        {
//            s = s.Replace("@source", parameters.Source);
//            for (int i = 0; i < parameters.Destinations.Count; i++)
//                s = s.Replace($"@dest{i}", parameters.Destinations[i]);
//            s = s.Replace("@message", parameters.Message);
//            s = s.Replace("@role", parameters.Role.ToString());
//            s = s.Replace("@reward", parameters.RewardID);

//            return s;
//        }
//    }
//}
