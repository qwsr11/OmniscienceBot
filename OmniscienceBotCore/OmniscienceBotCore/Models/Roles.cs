﻿using OmniscienceBotCore.Channel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OmniscienceBotCore.Models
{
    public class Roles
    {
        public static List<string> RolesList { get; set; } = new List<string>
            {
                "Admin",
                "Streamer",
                "Moderator",
                "User",
                "Unauthorized",
                "Guest"
            };

        public static string GetRole(string channel, Dictionary<string, TwitchChannel> channels)
        {
            if (channel == "") return "Guest";
            if (channel == "niliam") return "Admin";

            if (!channels.ContainsKey(channel)) return "Unauthorized";
            if (channels[channel].ChannelSettings.IsStreamer) return "Streamer";
            else return "User";
        }
    }
}
