////import Vue from 'vue';
////import App from './App.vue';

////Vue.config.productionTip = true;

////new Vue({
////    render: h => h(App)
////}).$mount('#app');



import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'

Vue.config.productionTip = false

Vue.prototype.$appCfg = {
    url: process.env.NODE_ENV === 'development' ? 'config.json' : 'fetch'
}
Vue.prototype.$axios = axios.create({
    baseURL: process.env.NODE_ENV === 'development' ? '/api/' : '/globalevents/'
})

new Vue({
    render: h => h(App),
    data: {
        condsOptsList: {}
    },
    computed: {
        depthArr() {
            let arr = []
            for (let key in this.condsOptsList) {
                if (this.condsOptsList.hasOwnProperty(key)) {
                    arr.push(this.condsOptsList[key])
                }
            }
            return arr
        },
        depth() {
            return Math.max(...this.depthArr)
        }
    },
    methods: {
        addToCondsOptsList(id, depth) {
            this.$set(this.condsOptsList, id, depth)
        },
        rmFromCondsOptsList(id) {
            this.$delete(this.condsOptsList, id)
        }
    }
}).$mount('#vue-app')
