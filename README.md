Twitch chatbot with discord and spotify integration.

Core functionality:
    Events and GlobalEvents - a mighty constructor for message processing rules and reactions on it.
    Commands - responses to user !input in a prescribed form. Also handles repeating messages to the twitch chat.
    Counters - a live-wise counter for the occurances of given phrase with specified reaction.
    Interactions - allows to write a message on behalf of the bot in your channel.
    Announcements - pushes a formatted announcement once a stream specified is started.
    Sub and gifted sub custom reactions.

Discord integration provided via non-repeating Commands and Announcements support.
Spotify can be accessed via both Events should the access token provided by user.

Thanks to @MrBarfi for the vue front-end part of Events contructor functionality.

OmniBot
